﻿using Control;
using Enum;
using Message;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class Action
{
    public string m_name;
    public Actions m_action;
    public bool m_isCard;
    public bool m_needTarget;

    public Action(Action p_ocard)
    {
        m_name = p_ocard.m_name;
        m_action = p_ocard.m_action;
        m_isCard = p_ocard.m_isCard;
    }

    public Action()
    {
    }

    public virtual void playAction()
    {
        Debug.Log("Base action");
    }

    [Command]
    public virtual void CmdPlayAction()
    {
        playAction();
        Debug.Log("CMD Base action for " + m_name);
    }

    public virtual void finalAction(Controller p_controller, PLAYER_ActionReq p_currentPlayerActionReq)
    {
        Debug.Log("Base final action");
    }

    public virtual void finalActionReverted(Controller p_controller, Players p_player)
    {
        Debug.Log("Base final action");
    }
}