﻿using System.Collections.Generic;
using Enum;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class CardGenerator
    {
        private const int c_maxNumberOfCards = 3;

        public Russia m_russia;
        public Police m_police;
        public Protest m_protest;
        public UE m_ue;
        public Media m_media;
        public ONZ m_onz;

        public bool m_useOnz = false;

        public List<Card> m_allCards;
        public Dictionary<Card, int> m_discarded;

        public void prepareCards()
        {
            m_allCards.Clear();
            add3CardsToList(m_russia);
            add3CardsToList(m_police);
            add3CardsToList(m_protest);
            add3CardsToList(m_ue);
            add3CardsToList(m_media);
            add3CardsToList(m_onz);
        }

        public delegate void DiscardedChanged(Dictionary<Card, int> p_discarded);

        public event DiscardedChanged OnDiscardedChanged;

        private void add3CardsToList<T>(T p_card) where T : Card
        {
            for (int i = 0; i < 3; ++i)
            {
                m_allCards.Add(p_card);
            }
        }

        public Cards givePlayerCards()
        {
            return new Cards(randomCard(), randomCard());
        }

        private Card randomCard()
        {
            Card card = m_allCards[Random.Range(0, m_allCards.Count)];
            card.m_isInGame = true;
            m_allCards.Remove(card);
            return card;
        }

        public void discard(Card p_card)
        {
            if (m_discarded == null)
                m_discarded = new Dictionary<Card, int>();
            if (m_discarded.ContainsKey(p_card))
                m_discarded[p_card]++;
            else m_discarded.Add(p_card, 1);
            if (OnDiscardedChanged != null)
            {
                OnDiscardedChanged(m_discarded);
            }
            p_card.m_isInGame = false;
        }

        public void putBackToDeck(Card p_card)
        {
            m_allCards.Add(p_card);
        }

        public Sprite getCardSprite(Actions p_action)
        {
            if (p_action.Equals(Actions.Russia)) return m_russia.m_texture;
            if (p_action.Equals(Actions.ONZ)) return m_onz.m_texture;
            if (p_action.Equals(Actions.Media)) return m_media.m_texture;
            if (p_action.Equals(Actions.Police)) return m_police.m_texture;
            if (p_action.Equals(Actions.Protest)) return m_protest.m_texture;
            if (p_action.Equals(Actions.UE)) return m_ue.m_texture;

            return null;
        }
    }
}