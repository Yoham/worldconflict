﻿using System.Collections;
using CORE.Server;
using Enum;
using Message;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class PLAYER_ActionReqFsm : CFSM
    {
        private PLAYER_ActionReq m_currentPlayerActionReq;
        private PLAYER_ActionResp m_currentPlayerActionResp;
        private OPLAYER_ReactionReq m_oPlayerReactionReq;
        private OPLAYER_ReactionResp m_oPlayerReactionResp;

        public int m_responseCount = 0;

        public PLAYER_ActionReqFsm(Controller p_controller) : base()
        {
            m_mono = p_controller;
            m_controller = p_controller;
        }

        public override IEnumerator stateMachine(object p_message)
        {
            markReceived();

            if (State.Equals(States.Idle))
            {
                handleStateIdle(p_message);
            }

            yield break;
        }

        protected override void handleStateIdle(object p_message)
        {
            PLAYER_ActionReq l_playerActionReq = (PLAYER_ActionReq)p_message;
            m_currentPlayerActionReq = l_playerActionReq;

            OPLAYER_ReactionReq l_oPlayerActionReq = new OPLAYER_ReactionReq(l_playerActionReq);
            m_oPlayerReactionReq = l_oPlayerActionReq;

            if (l_playerActionReq.playerId.Equals(Players.Player1))
            {
                foreach (PlayerHand l_ph in m_controller.m_oPlayers)
                {
                    sendMessage<OPLAYER_ReactionReq>(l_oPlayerActionReq, l_ph.m_playerId);
                    m_responseCount++;
                }
            }
            else
            {
                foreach (PlayerHand l_ph in m_controller.m_players)
                {
                    if (!l_ph.m_playerId.Equals(l_playerActionReq.playerId))
                    {
                        sendMessage<OPLAYER_ReactionReq>(l_oPlayerActionReq, l_ph.m_playerId);
                        m_responseCount++;
                    }
                    
                }
                //sendMessage<OPLAYER_ReactionReq>(l_oPlayerActionReq, Players.Player1);
            }



            stateTransition(States.WaitingForResponse);
        }

    }
}