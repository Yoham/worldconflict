﻿using System.Collections;
using Enum;
using Message;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class OPLAYER_FinalActionReqFsm : CFSM
    {
        private PLAYER_FinalActionReq m_currentPlayerFinalActionReq;
        private PLAYER_FinalActionResp m_currentPlayerFinalActionResp;
        private OPLAYER_FinalActionReq m_oPlayerFinalActionReq;
        private OPLAYER_FinalActionResp m_oPlayerFinalActionResp;

        public int m_responseCount = 0;

        public OPLAYER_FinalActionReqFsm(Controller p_controller) : base()
        {
            m_mono = p_controller;
            m_controller = p_controller;
        }

        public override IEnumerator stateMachine(object p_message)
        {
            markReceived();

            if (State.Equals(States.Idle))
            {
                handleStateIdle(p_message);
            }

            yield break;
        }

        protected override void handleStateIdle(object p_message)
        {
            PLAYER_FinalActionReq l_playerFinalActionReq = (PLAYER_FinalActionReq)p_message;
            m_currentPlayerFinalActionReq = l_playerFinalActionReq;

            OPLAYER_FinalActionReq l_oPlayerFinalActionReq = new OPLAYER_FinalActionReq(l_playerFinalActionReq);
            m_oPlayerFinalActionReq = l_oPlayerFinalActionReq;

            if (l_playerFinalActionReq.playerId.Equals(Players.Player1))
            {
                foreach (PlayerHand l_ph in m_controller.m_oPlayers)
                {
                    sendMessage<OPLAYER_FinalActionReq>(l_oPlayerFinalActionReq, l_ph.m_playerId);
                    m_responseCount++;
                }
            }
            else
            {

                sendMessage<OPLAYER_FinalActionReq>(l_oPlayerFinalActionReq, Players.Player1);

                //sendMessage<OPLAYER_ReactionReq>(l_oPlayerActionReq, Players.Player1);
            }


            stateTransition(States.WaitingForResponse);
        }

    }
}