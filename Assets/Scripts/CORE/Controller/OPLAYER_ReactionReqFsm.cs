﻿using System.Collections;
using System.Collections.Generic;
using Enum;
using Message;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class OPLAYER_ReactionReqFsm : CFSM
    {
        private PLAYER_ActionReq m_currentPlayerActionReq;
        private PLAYER_ActionResp m_currentPlayerActionResp;
        private OPLAYER_ReactionReq m_oPlayerReactionReq;
        private OPLAYER_ReactionResp m_oPlayerReactionResp;

        private int m_responseCount = 0;
        private List<Players> m_lockedPlayers = new List<Players>();

        public OPLAYER_ReactionReqFsm(Controller p_controller) : base()
        {
            m_mono = p_controller;
            m_controller = p_controller;
        }

        public override IEnumerator stateMachine(object p_message)
        {
            markReceived();

            if (!locked)
            {

                if (State.Equals(States.Idle))
                {
                    handleStateIdle(p_message);
                }

                if (State.Equals(States.WaitingForResponse))
                {
                    while (true)
                    {
                        locked = true;
                        FsmCoroutine c = new FsmCoroutine(m_mono, getMessage<PLAYER_DisableReactionResp>());
                        yield return c.coroutine;
                        if (c.result != null)
                        {
                            Debug.Log("Current responses left: " + m_responseCount);
                            if (c.result is PLAYER_DisableReactionResp && m_responseCount == 0)
                            {
                                handleStateWaitingForResponse(c.result);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                Debug.LogWarning("Locked so ignoring " + p_message.GetType());
            }

            yield break;
        }

        protected override void handleStateIdle(object p_message)
        {
            OPLAYER_ReactionResp l_oplayerReactionResp = (OPLAYER_ReactionResp)p_message;
            m_oPlayerReactionResp = l_oplayerReactionResp;

            if (l_oplayerReactionResp.wasTargeted)
            {
                if (!l_oplayerReactionResp.playerId.Equals(Players.Player1))
                {
                    foreach (PlayerHand l_ph in m_controller.m_oPlayers)
                    {
                        PLAYER_DisableReactionReq l_playerDisableReactionReq = new PLAYER_DisableReactionReq(l_ph.m_playerId);
                        sendMessage<PLAYER_DisableReactionReq>(l_playerDisableReactionReq, l_ph.m_playerId);
                        m_responseCount++;
                    }
                }
                else
                {
                    PLAYER_DisableReactionReq l_playerDisableReactionReq = new PLAYER_DisableReactionReq(Players.Player1);
                    sendMessage<PLAYER_DisableReactionReq>(l_playerDisableReactionReq, Players.Player1);
                    m_responseCount++;



                    //sendMessage<OPLAYER_ReactionReq>(l_oPlayerActionReq, Players.Player1);
                }


                stateTransition(States.WaitingForResponse);
            }
            else
            {
                if (!l_oplayerReactionResp.oPlayerReaction.Equals(Reactions.Pass))
                {

                    if (!l_oplayerReactionResp.playerId.Equals(Players.Player1))
                    {
                        foreach (PlayerHand l_ph in m_controller.m_oPlayers)
                        {
                            PLAYER_DisableReactionReq l_playerDisableReactionReq = new PLAYER_DisableReactionReq(l_ph.m_playerId);
                            sendMessage<PLAYER_DisableReactionReq>(l_playerDisableReactionReq, l_ph.m_playerId);
                            m_responseCount++;
                        }
                    }
                    else
                    {
                        PLAYER_DisableReactionReq l_playerDisableReactionReq = new PLAYER_DisableReactionReq(Players.Player1);
                        sendMessage<PLAYER_DisableReactionReq>(l_playerDisableReactionReq, Players.Player1);
                        m_responseCount++;

                        //sendMessage<OPLAYER_ReactionReq>(l_oPlayerActionReq, Players.Player1);
                    }

                    stateTransition(States.WaitingForResponse);
                }
                else
                {
                    PLAYER_DisableReactionReq l_playerDisableReactionReq = new PLAYER_DisableReactionReq(l_oplayerReactionResp.playerId);
                    if (!l_oplayerReactionResp.playerId.Equals(Players.Player1))
                    {
                        sendMessage<PLAYER_DisableReactionReq>(l_playerDisableReactionReq, l_oplayerReactionResp.playerId);

                    }
                    else
                    {
                        sendMessage<PLAYER_DisableReactionReq>(l_playerDisableReactionReq, Players.Player1);
                    }
                    m_responseCount++;
                    m_lockedPlayers.Add(l_oplayerReactionResp.playerId);
                    //TODO should be idle
                    stateTransition(States.WaitingForResponse);
                }
            }
            locked = false;
        }

        protected override void handleStateWaitingForResponse(object p_message)
        {
            PLAYER_DisableReactionResp l_playerDisableReactionResp = (PLAYER_DisableReactionResp)p_message;
            OPLAYER_ReactionResp l_oPlayerReactionResp = m_oPlayerReactionResp;

            if (!l_oPlayerReactionResp.oPlayerReaction.Equals(Reactions.Pass))
            {
                PLAYER_ActionResp l_playerActionResp = new PLAYER_ActionResp(m_currentPlayerActionReq);

                l_playerActionResp.playerReaction = l_oPlayerReactionResp.oPlayerReaction;
                l_playerActionResp.responseAction = l_oPlayerReactionResp.responseAction;
                l_playerActionResp.playerId = l_oPlayerReactionResp.targetID;
                l_playerActionResp.targetID = l_oPlayerReactionResp.playerId;
                m_currentPlayerActionResp = l_playerActionResp;

                if (l_oPlayerReactionResp.oPlayerReaction.Equals(Reactions.Block))
                {


                }
                else
                {


                }


                sendMessage<PLAYER_ActionResp>(l_playerActionResp, l_playerActionResp.playerId);
            }
            else
            {
                PLAYER_ActionResp l_playerActionResp = new PLAYER_ActionResp(m_currentPlayerActionReq);

                l_playerActionResp.playerReaction = l_oPlayerReactionResp.oPlayerReaction;
                l_playerActionResp.responseAction = l_oPlayerReactionResp.responseAction;
                l_playerActionResp.playerId = l_oPlayerReactionResp.targetID;
                l_playerActionResp.targetID = l_oPlayerReactionResp.playerId;
                m_currentPlayerActionResp = l_playerActionResp;

                //if (l_oPlayerReactionResp.playerId.Equals(Players.Player1))
                //{
                sendMessage<PLAYER_ActionResp>(l_playerActionResp, l_playerActionResp.playerId);
                //}
                //else
                //{
                //    sendMessage<PLAYER_ActionResp>(l_playerActionResp, Players.Player2);
                //}
            }
            stateTransition(States.ProceedingResponse);
        }

        protected override IEnumerator getMessage<T>()
        {
            object l_received = ServerPort.receive<object>();
            yield return l_received;
            if (l_received != null)
            {
                if (l_received.GetType() == typeof(T))
                {
                    markReceived();
                    --m_responseCount;
                }
            }
            yield break;



        }

    }
}