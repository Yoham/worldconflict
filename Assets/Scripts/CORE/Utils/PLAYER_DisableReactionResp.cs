﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct PLAYER_DisableReactionResp
    {
        public bool hasDisabledControls;
        public Players playerID;


        public PLAYER_DisableReactionResp(PLAYER_DisableReactionReq p_playerDisableReactionReq) : this()
        {
            this.hasDisabledControls = true;
            this.playerID = p_playerDisableReactionReq.playerID;
        }

        public override string ToString()
        {
            return "hasDisabledControls = " + hasDisabledControls.ToString() + " \n" +
                   "playerId = " + playerID.ToString();
        }
    }
}