﻿namespace Message
{
    [System.Serializable]
    public struct PLAYER_FinalActionAck
    {
        public bool acknowledged;

        public PLAYER_FinalActionAck(bool acknowledged) : this()
        {
            this.acknowledged = true;
        }

        public override string ToString()
        {
            return "acknowledged = " + acknowledged.ToString() + " \n";
        }
    }
}