﻿namespace Enum
{
    [System.Serializable]
    public enum States
    {
        Idle,
        WaitingForResponse,
        ProceedingResponse
    }

    [System.Serializable]
    public enum Reactions
    {
        Pass,
        Check,
        Block
    }

    [System.Serializable]
    public enum Players
    {
        Player1,
        Player2,
        Player3,
        Player4,
        Player5,
        Player6,
        UNSUPPORTED
    }

    [System.Serializable]
    public enum Messages
    {
		CONTROLLER_AssignId,
        CONTROLLER_TurnMasterRightsInd,
        CONTROLLER_TurnEnd,
        OPLAYER_ReactionReq,
        OPLAYER_ReactionResp,
        PLAYER_ActionReq,
        PLAYER_ActionResp,
        PLAYER_DisableReactionReq,
        PLAYER_DisableReactionResp,
        PLAYER_FinalActionAck,
        PLAYER_FinalActionReq,
        OPLAYER_FinalActionReq,
        PLAYER_FinalActionResp,
        OPLAYER_FinalActionResp,
        NONE
    }

    [System.Serializable]
    public enum Actions
    {
        USA,
        LocalBuisiness,
        Swindle,
        Police,
        Russia,
        Protest,
        UE,
        Media,
        ONZ,
        Pass
    }
}