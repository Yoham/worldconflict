﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct PLAYER_FinalActionResp
    {
        public Actions responseAction;
        public Reactions playerReaction;
        public Players playerId;
        public Players targetID;

        public PLAYER_FinalActionResp(OPLAYER_FinalActionResp p_playerFinalActionReq) : this()
        {
            this.playerReaction = p_playerFinalActionReq.playerReaction;
            this.responseAction = p_playerFinalActionReq.responseAction;
            this.playerId = p_playerFinalActionReq.playerId;
            this.targetID = p_playerFinalActionReq.targetID;
        }

        public override string ToString()
        {
            return "responseAction = " + responseAction.ToString() + " \n" +
                   "playerReaction = " + playerReaction.ToString() + " \n" +
                   "playerId = " + playerId.ToString() + " \n" +
                   "targetID = " + targetID.ToString();

        }
    }
}