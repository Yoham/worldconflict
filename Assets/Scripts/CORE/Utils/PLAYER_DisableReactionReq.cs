﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct PLAYER_DisableReactionReq
    {
        public Players playerID;


        public PLAYER_DisableReactionReq(Players p_player) : this()
        {
            this.playerID = p_player;
        }

        public override string ToString()
        {
            return "playerID = " + playerID.ToString();
        }
    }
}