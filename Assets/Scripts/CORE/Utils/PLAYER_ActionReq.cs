using Enum;

namespace Message
{
    [System.Serializable]
    public struct PLAYER_ActionReq
    {
        public Actions playedAction;
        public Players playerId;
        public Players targetID;

        public PLAYER_ActionReq(Actions p_action) : this()
        {
            this.playedAction = p_action;
            this.playerId = Players.Player1;
            this.targetID = Players.Player2;
        }

        public PLAYER_ActionReq(PlayerHand p_player) : this()
        {
            this.playedAction = p_player.m_selectedAction;
            this.playerId = p_player.m_playerId;
            this.targetID = p_player.m_targetedPlayerId;
        }

        public override string ToString()
        {
            return "playedAction = " + playedAction.ToString() + " \n" +
                   "playerId = " + playerId.ToString() + " \n" +
                   "targetID = " + targetID.ToString();
        }
    }
}