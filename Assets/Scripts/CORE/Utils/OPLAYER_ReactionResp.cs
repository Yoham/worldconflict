﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct OPLAYER_ReactionResp
    {
        public Reactions oPlayerReaction;
        public Actions responseAction;
        public bool wasTargeted;
        public Players playerId;
        public Players targetID;


        public OPLAYER_ReactionResp(OPLAYER_ReactionReq p_oPlayerReactionReq) : this()
        {
            this.oPlayerReaction = Reactions.Pass;
            this.responseAction = Actions.Pass;
            this.wasTargeted = true;
            this.playerId = p_oPlayerReactionReq.targetID;
            this.targetID = p_oPlayerReactionReq.playerId; ;
        }

        public override string ToString()
        {
            return "oPlayerReaction = " + oPlayerReaction.ToString() + " \n" +
                   "responseAction = " + responseAction.ToString() + " \n" +
                   "playerId = " + playerId.ToString() + " \n" +
                   "targetID = " + targetID.ToString();
        }

    }
}