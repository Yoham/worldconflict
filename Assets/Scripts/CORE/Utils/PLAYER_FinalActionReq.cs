﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct PLAYER_FinalActionReq
    {
		public Actions responseAction;
		public Reactions playerReaction;
        public Players playerId;
        public Players targetID;

        public PLAYER_FinalActionReq(PLAYER_ActionResp p_playerActionResp) : this()
        {
            this.playerReaction = Reactions.Pass;
            this.responseAction = p_playerActionResp.responseAction;
            this.playerId = p_playerActionResp.playerId;
            this.targetID = p_playerActionResp.targetID;
        }

        public override string ToString()
        {
			return "responseAction = " + responseAction.ToString() + " \n" +
                   "playerReaction = " + playerReaction.ToString() + " \n" +
                   "playerId = " + playerId.ToString() + " \n" +
                   "targetID = " + targetID.ToString();

        }
    }
}