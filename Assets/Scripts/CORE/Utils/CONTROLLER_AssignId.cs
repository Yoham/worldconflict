﻿using Enum;

namespace Message
{
	[System.Serializable]
	public struct CONTROLLER_AssignId
	{
		public Players playerID;
        public Actions cardsInHand1;
        public Actions cardsInHand2;


	    public CONTROLLER_AssignId(Players p_playerID, Actions p_card1, Actions p_card2) : this()
	    {
	        this.playerID = p_playerID;
	        cardsInHand1 = p_card1;
	        cardsInHand2 = p_card2;
	    }

	    public override string ToString()
		{
			return "playerID = " + playerID.ToString() + " \n" +
                   "cardsInHand1 "+ cardsInHand1.ToString() + "cardsInHand2 " + cardsInHand2.ToString();
		}
	}
}