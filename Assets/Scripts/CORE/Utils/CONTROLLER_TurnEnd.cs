﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct CONTROLLER_TurnEnd
    {
        public Players playerID;
        public bool turnEnd;

        public CONTROLLER_TurnEnd(Players p_player,bool p_turnEnd=true) : this()
        {
            this.playerID = p_player;
            this.turnEnd = p_turnEnd;
        }

        public override string ToString()
        {
            return "playerID = " + playerID.ToString() + " \n"+
                   "turnEnd = " + turnEnd.ToString() + " \n";
        }
    }
}