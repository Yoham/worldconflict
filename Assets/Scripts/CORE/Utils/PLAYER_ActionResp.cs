﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct PLAYER_ActionResp
    {
        public Reactions playerReaction;
        public Actions responseAction;
        public Players playerId;
        public Players targetID;

        public PLAYER_ActionResp(Actions p_action) : this()
        {
            this.playerReaction = Reactions.Pass;
            this.responseAction = p_action;
            this.playerId = Players.Player2;
            this.targetID = Players.Player1;
        }

        //Test only for now
        public PLAYER_ActionResp(PLAYER_ActionReq p_actionReq) : this()
        {
            this.playerReaction = Reactions.Pass;
            this.responseAction = Actions.Pass;
            this.playerId = p_actionReq.targetID;
            this.targetID = p_actionReq.playerId;
        }

        public override string ToString()
        {
            return "playerReaction = " + playerReaction.ToString() + " \n" +
                   "responseAction = " + responseAction.ToString() + " \n" +
                   "playerId = " + playerId.ToString() + " \n" +
                   "targetID = " + targetID.ToString();
        }
    }
}