﻿using System.Runtime.CompilerServices;

namespace Message
{
    [System.Serializable]
    public struct MessageBase
    {
        public string type;
        public string jsonMessage;

        public MessageBase(string p_type, string p_jsonMessage) :this()
        {
            type = p_type;
            jsonMessage = p_jsonMessage;
        }

    }
}