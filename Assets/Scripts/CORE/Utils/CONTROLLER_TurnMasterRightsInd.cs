﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct CONTROLLER_TurnMasterRightsInd
    {
        public Players playerID;

        public CONTROLLER_TurnMasterRightsInd(Players p_playerID) : this()
        {
            this.playerID = p_playerID;
        }

        public override string ToString()
        {
            return "playerID = " + playerID.ToString() + " \n";
        }
    }
}