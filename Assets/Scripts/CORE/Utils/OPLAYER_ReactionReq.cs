﻿using Enum;

namespace Message
{
    [System.Serializable]
    public struct OPLAYER_ReactionReq
    {

        public Actions playedAction;
        public Players playerId;
        public Players targetID;

        /*public OPLAYER_ReactionReq(Actions p_action) : this()
        {
            this.playedAction = p_action;
            this.playerId = Players.Player1;
            this.targetID = Players.Player2;
        }
        **/
        public OPLAYER_ReactionReq(PLAYER_ActionReq p_playerActionReq) : this()
        {
            this.playedAction = p_playerActionReq.playedAction;
            this.playerId = p_playerActionReq.playerId;
            this.targetID = p_playerActionReq.targetID;
        }

        public override string ToString()
        {
            return "playedAction = " + playedAction.ToString() + " \n" +
                   "playerId = " + playerId.ToString() + " \n" +
                   "targetID = " + targetID.ToString();
        }
    }
}