using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Enum;
using Message;
using Newtonsoft.Json;
using UnityEngine;

namespace CORE.Server
{
    public class Server
    {
        private bool m_peerListening;
        private bool m_messageListening;
        private Thread m_connectionListenerThread;
        private TcpListener m_tcpListener = null;
        private ConnectionContainer m_connection;

        private Dictionary<IPAddress, ConnectionContainer> m_connectedClients = new Dictionary<IPAddress, ConnectionContainer>();
        private List<ConnectionContainer> l_list = new List<ConnectionContainer>();
        private int m_maxPlayers = 1;
        public delegate void PlayerChanged();
        public event PlayerChanged onPlayerChanged;

        public int maxPlayers
        {
            get { return m_maxPlayers; }
            set
            {
                m_maxPlayers = value;
                if (onPlayerChanged != null)
                {
                    onPlayerChanged();
                }
            }
        }

        private void prepareThread()
        {
            ThreadStart l_threadFunction = new ThreadStart(sayHello);
            m_connectionListenerThread = new Thread(l_threadFunction);
            m_connectionListenerThread.Start();
        }

        public void startServ()
        {
            m_peerListening = true;
            m_messageListening = false;
            prepareThread();
        }

        public void stopListening()
        {
            m_peerListening = false;

        }

        private void sayHello()
        {
            try
            {
                string l_strHostName = Dns.GetHostName();
                IPHostEntry l_ipEntry = Dns.GetHostEntry(l_strHostName);
                IPAddress[] l_addr = l_ipEntry.AddressList;
                string l_ipAddr = l_addr[0].ToString();
                const int port = 60066;

                m_tcpListener = new TcpListener(IPAddress.Parse(l_ipAddr), port);
                m_tcpListener.Start();
                Debug.Log("Server: Server is listening at " + l_ipAddr + ":" + port);
                while (m_peerListening)
                {
                    if (m_connectedClients.Count == m_maxPlayers)
                    {
                        stopListening();
                    }

                    if (!m_tcpListener.Pending())
                    {
                        Thread.Sleep(100);
                    }
                    else
                    {
                        ConnectionContainer l_connection = new ConnectionContainer(m_tcpListener.AcceptTcpClient());
                        m_connection = l_connection;
                        if (m_connection.client.Connected)
                        {
                            IPEndPoint l_ipep = (IPEndPoint)m_connection.client.Client.RemoteEndPoint;
                            IPAddress l_ipa = l_ipep.Address;

                            if (!m_connectedClients.ContainsKey(l_ipa))
                            {
								Debug.Log("Server: Client " + l_ipa + " connected!");
								m_connection.playerID = Players.Player2;
                                m_connectedClients.Add(l_ipa, m_connection);
                                l_list.Add(l_connection);
                                //ConnectionHandler.I.m_controller.addPlayerToGame();
                            }
							//ServerPort.send(new CONTROLLER_AssignId(Players.Player2));
                            //m_connection.sendMessage("Hello from server! You are Player1");
                        }
                    }
                }
            }
            catch (ThreadAbortException e)
            {
                Debug.Log(e);
            }
            finally
            {
                m_peerListening = false;
                m_messageListening = true;
                m_tcpListener.Stop();

            }

            messageListener();
            ConnectionHandler.I.m_startNewGame = true;
            Debug.Log("Server: After listener");
        }

        private void messageListener()
        {

            m_messageListening = true;
            //prepareThread();
            m_connection.startListeningThread(messageListenerLoop);

        }

        private void messageListenerLoop()
        {
            while (m_messageListening)
            {
                try
                {
                    m_connection.receiveMessage();
                    if (m_connection.m_captured != null)
                        ServerPort.enqueue(m_connection.m_captured);

                }
                catch (Exception e)
                {
                    break;
                }

            }
        }

        public void send(string p_str)
        {
            if (m_connection != null)
                m_connection.sendMessage(p_str);
        }

        public void sendAll()
        {
            foreach (ConnectionContainer l_connection in l_list)
            {
                Debug.Log("Sending to " + l_connection.ip);
                try
                {
                    l_connection.sendMessage("Hi guys, i sent it to both of you");

                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }

            }
        }

        public void OnApplicationQuit()
        {
            // stop listening thread
            Debug.Log("Server: Quit");
            m_messageListening = false;
            stopListening();

            foreach (ConnectionContainer l_connection in m_connectedClients.Values)
            {
                l_connection.closeConnection();//client.Close();
            }
            // wait fpr listening thread to terminate (max. 500ms)
            if (m_connectionListenerThread != null)
            {
                m_connectionListenerThread.Join(500);
                m_connectionListenerThread.Abort();
            }

        }
    }
}