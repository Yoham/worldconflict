﻿using Control;
using CORE.Server;
using UnityEngine;

namespace CORE.Server
{
    public class ConnectionHandler : MonoBehaviour
    {
        #region Singleton

        private static ConnectionHandler s_instance;

        public static bool isActive
        {
            get { return s_instance != null; }
        }

        public static ConnectionHandler I
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = UnityEngine.Object.FindObjectOfType(typeof (ConnectionHandler)) as ConnectionHandler;

                    if (s_instance == null)
                    {
                        GameObject go = new GameObject("Connection");
                        DontDestroyOnLoad(go);
                        s_instance = go.AddComponent<ConnectionHandler>();
                    }
                }
                return s_instance;
            }
        }

        

        #endregion Singleton
        public Server m_server = new Server();
        public ServerConnection m_client = new ServerConnection();
        public Controller m_controller;

        public bool m_isHost = false;
        public bool m_startNewGame = false;
        public bool m_cardReceived = false;

        public global::CORE.Server.Server server
        {
            get { return m_server; }
        }

        public ServerConnection client
        {
            get { return m_client; }
        }

        private void Start()
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}