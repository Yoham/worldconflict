using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Enum;
using Message;
using Newtonsoft.Json;
using UnityEngine;

namespace CORE.Server
{
    public class ServerConnection
    {
        private ConnectionContainer m_connection;


        public void ConnectToServer(string p_ip)
        {
            string l_ipAddr = p_ip;
            const int port = 60066;
            m_connection = new ConnectionContainer(new TcpClient(l_ipAddr, port));
            try
            {
                m_connection.client.Connect(l_ipAddr, port);
            }
            catch
            {
                Debug.Log("Client: Connection refused! Cannot find remote host at " + l_ipAddr);
            }

            if (m_connection.client.Connected)
            {
                Debug.Log("Client: Connected to server at " + l_ipAddr);
                startListeningForServerMessages();
                ConnectionHandler.I.m_startNewGame = true;
                //ConnectionHandler.I.m_controller.addPlayerToGame();
            }
        }

        private void startListeningForServerMessages()
        {
            m_connection.startListening();//startListeningThread(loop);
        }

        public void send(string p_str)
        {
            if (m_connection != null)
                m_connection.sendMessage(p_str);
        }

        public void OnApplicationQuit()
        {
            Debug.Log("Client: Quit");
            if (m_connection != null)
                m_connection.closeConnection();
        }

    }
}