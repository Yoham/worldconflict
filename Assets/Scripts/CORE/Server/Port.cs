﻿using System.Collections.Generic;
using CORE.Server;
using Enum;
using Message;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public static class ClientPort 
{
    public static Queue<object> s_queue = new Queue<object>();

    public static void send(object p_obj, Players p_player)
    {
        if (p_player.Equals(Players.Player1))
        {
            ServerPort.s_queue.Enqueue(p_obj);
        }
        else
        {
            string l_message = JsonConvert.SerializeObject(p_obj);
            string l_type = p_obj.GetType().ToString();
            MessageBase l_messageBase = new MessageBase(l_type, l_message);
            string l_messageToSend = JsonConvert.SerializeObject(l_messageBase);
            Debug.LogWarning(l_messageToSend);
            ConnectionHandler.I.m_client.send(l_messageToSend);
        }
    }

    public static void enqueue(object p_obj)
    {
        s_queue.Enqueue(p_obj);
    }

    public static T receive<T>()
    {
        if (s_queue.Count > 0)
        {
            //Debug.Log("Receive!");
            T l_it = (T)s_queue.Peek();
            //Debug.Log((l_it.ToString()));
            if (l_it != null)
            {
                //s_queue.Dequeue();
                return l_it;
            }
        }
        return default(T);
    }

    public static void markProceeded()
    {
        Debug.Log("UReceived " + s_queue.Peek().GetType() + " " + s_queue.Peek().ToString());
        s_queue.Dequeue();
    }
}

[System.Serializable]
public static class ServerPort
{
    public static Queue<object> s_queue = new Queue<object>();

    public static void send(object p_obj, Players p_player)
    {
        if (p_player.Equals(Players.Player1))
        {
            ClientPort.s_queue.Enqueue(p_obj);
        }
        else
        {
            string l_message = JsonConvert.SerializeObject(p_obj);
            string l_type = p_obj.GetType().ToString();
            MessageBase l_messageBase = new MessageBase(l_type, l_message);
            string l_messageToSend = JsonConvert.SerializeObject(l_messageBase);
            Debug.LogWarning(l_messageToSend);
            ConnectionHandler.I.m_server.send(l_messageToSend);
        }
    }

    public static void enqueue(object p_obj)
    {
        s_queue.Enqueue(p_obj);
    }

    public static T receive<T>()
    {
        if (s_queue.Count > 0)
        {
            //Debug.Log("Receive!");
            T l_it = (T)s_queue.Peek();
            //Debug.Log((l_it.ToString()));
            if (l_it != null)
            {
                //s_queue.Dequeue();
                return l_it;
            }
        }
        return default(T);
    }

    public static void markProceeded()
    {
        Debug.Log("CReceived " + s_queue.Peek().GetType() + " " + s_queue.Peek().ToString());
        s_queue.Dequeue();
    }
}