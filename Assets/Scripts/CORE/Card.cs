﻿using Control;
using Enum;
using UnityEngine;

[System.Serializable]
public class Card : Action
{
    #region Public_Fields

    public Sprite m_texture;
    public int m_value;
    public bool m_isInGame;
    public delegate void CardChanged(Card p_cards);
    public event CardChanged OnCardChanged;

    public Card()
    {
        m_isCard = true;
    }

    public Card(Card p_ocard) : base(p_ocard)
    {
        m_name = p_ocard.m_name;
        m_texture = p_ocard.m_texture;
        m_value = p_ocard.m_value;
        m_action = p_ocard.m_action;
        m_isCard = true;
    }

    public Card(Actions p_1)
    {
        Card l_card=Utils.getCardType(p_1);
        m_name = l_card.m_name;
        m_texture = l_card.m_texture;
        m_value = l_card.m_value;
        m_action = l_card.m_action;
        m_isCard = true;
    }

    #endregion Public_Fields

    public override void playAction()
    {
        Debug.Log("Action");
    }

    public virtual void discard()
    {
        Debug.Log("Discard base");
        if (OnCardChanged != null)
            OnCardChanged(this);
        GameObject.FindObjectOfType<Controller>().m_cardGenerator.discard(this);
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public bool Equals(Card obj)
    {
        return m_name.Equals(obj.m_name);
    }
}