﻿using System;
using Control;
using Enum;
using System.Collections;
using CORE.Server;
using Message;
using UnityEngine;

[System.Serializable]
public class PlayerHand : MonoBehaviour
{
    #region Fields

    #region Delegates
    public delegate void MoneyChanged();
    public event MoneyChanged OnMoneyChanged;

    public delegate void PlayedCardChanged();
    public event PlayedCardChanged OnPlayedCardChanged;

    public delegate void ControlStateChanged(bool p_isControlEnabled);
    public event ControlStateChanged OnControlStateChanged;
    #endregion


    public bool hasCards { get; set; }
    public Cards m_cardsInHand;
    public Players m_playerId;
    public Players m_targetedPlayerId;
    public Actions m_selectedAction = Actions.Pass;

    public Messages m_currentMessage = Messages.NONE;
    public Reactions m_selectedReaction = Reactions.Pass;

    private bool m_isControlEnabled = false;
    public bool m_hasMasterRights = false;
    public object m_currentMessageObject = null;

    public BaseFsm m_oplayerReactionReqFsm;
    public BaseFsm m_playerDisableReactionReqFsm;

    private int m_money;

    public int Money
    {
        get { return m_money; }
        set
        {
            m_money = value;
            if (OnMoneyChanged != null)
            {
                OnMoneyChanged();
            }
        }
    }

    private Actions m_playedActions;
    private Players m_playedActionsTarget;
    public Card m_cardToDiscard;

    public Actions PlayedActions
    {
        get { return m_playedActions; }
        set
        {
            m_playedActions = value;
            if (OnPlayedCardChanged != null)
            {
                OnPlayedCardChanged();
            }
        }
    }

    public Players PlayedActionsTarget
    {
        get;
        set;
    }

    public bool ControlEnabled
    {
        get { return m_isControlEnabled; }
        set
        {

            if (m_isControlEnabled == value)
            {
            }
            else
            {
                Debug.LogWarning(m_playerId + " Change from " + m_isControlEnabled + " to " + value);
                m_isControlEnabled = value;
                if (OnControlStateChanged != null)
                {
                    OnControlStateChanged(m_isControlEnabled);
                }
            }
        }
    }

    public Controller m_controller { get; set; }
    private BaseFsm m_playerActionReqHandlerFsm;
    private BaseFsm m_playerFinalActionReqHandlerFsm;
    #endregion

    #region FSM_behaviour

    private void handleMessages(Messages p_messageType, object p_message)
    {
        switch (p_messageType)
        {
            case Messages.CONTROLLER_AssignId:
                CONTROLLER_AssignId l_controllerAssignId = (CONTROLLER_AssignId)p_message;
                //m_controller.addPlayerToGame(l_controllerAssignId.playerID);
                if (m_playerId.Equals(l_controllerAssignId.playerID))
                {
                    Debug.Log("PlayerHand: Assigned player ID cards for: " + l_controllerAssignId.playerID + " ");
                    m_cardsInHand.First = m_controller.getCardType(l_controllerAssignId.cardsInHand1);
                    m_cardsInHand.Second = m_controller.getCardType(l_controllerAssignId.cardsInHand2);
                }
                else
                {
                    PlayerHand l_oPlayer = Utils.getPlayer(l_controllerAssignId.playerID,m_controller.m_players);
                    l_oPlayer.m_cardsInHand.First = m_controller.getCardType(l_controllerAssignId.cardsInHand1);
                    l_oPlayer.m_cardsInHand.Second = m_controller.getCardType(l_controllerAssignId.cardsInHand2);
                    ConnectionHandler.I.m_cardReceived = true;
                }


                ClientPort.markProceeded();
                break;

            case Messages.CONTROLLER_TurnMasterRightsInd:
                handleAccesToTurnMasterRights(p_message);
                break;

            case Messages.OPLAYER_ReactionReq:
                handleReactionReq(p_message);
                break;

            case Messages.PLAYER_DisableReactionReq:
                handleDisableReactionReq(p_message);
                break;

            case Messages.OPLAYER_FinalActionReq:
                handleOFinalActionReq(p_message);
                break;

            case Messages.PLAYER_FinalActionResp:
                handleFinalActionResp(p_message);
                break;

            case Messages.CONTROLLER_TurnEnd:
                handleTurnEnd(p_message);
                break;

            case Messages.NONE:
                break;

            default:
                break;
        }
    }

    private IEnumerator refreshReferences()
    {
        m_controller = FindObjectOfType<Controller>();
        m_playerActionReqHandlerFsm = new PLAYER_ActionReqHandlerFsm(this);
        m_playerFinalActionReqHandlerFsm = new PLAYER_FinalActionReqHandlerFsm(this);
        yield return null;
        yield return null;

        if (OnMoneyChanged != null)
        {
            OnMoneyChanged();
        }
        if (OnControlStateChanged != null)
        {
            OnControlStateChanged(m_isControlEnabled);
        }
        yield break;
    }

    private void Start()
    {
        StartCoroutine(refreshReferences());
        setControlState(false);
    }


    private void Update()
    {

        object l_received = ClientPort.receive<object>();
        if (l_received != null)
        {
            Debug.Log("l_received = " + l_received.GetType() + " \n " + l_received.ToString());
            Messages l_currentMessageType = Utils.getMessageType(l_received);
            handleMessages(l_currentMessageType, l_received);

        }
    }

    #endregion

    #region Player_dependent_actions

    public void playStateBasedAction()
    {
        switch (m_currentMessage)
        {
            case Messages.CONTROLLER_TurnMasterRightsInd:
                sendPlayerActionReq();
                break;
            case Messages.CONTROLLER_TurnEnd:
                break;
            case Messages.OPLAYER_ReactionReq:
                sendOPlayerReactionResp();
                break;
            case Messages.OPLAYER_ReactionResp:
                break;
            case Messages.PLAYER_ActionReq:
                break;
            case Messages.PLAYER_ActionResp:
                sendPlayerFinalActionReq();
                break;
            case Messages.PLAYER_DisableReactionReq:
                break;
            case Messages.PLAYER_DisableReactionResp:
                break;
            case Messages.PLAYER_FinalActionAck:
                break;
            case Messages.PLAYER_FinalActionReq:
                break;
            case Messages.OPLAYER_FinalActionReq:
                sendPlayerFinalActionResp();
                break;
            case Messages.PLAYER_FinalActionResp:
                break;
            case Messages.OPLAYER_FinalActionResp:
                break;
            case Messages.NONE:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void playAction()
    {
        //Debug.Log("PState1");
        //StartCoroutine(m_playerActionReqHandlerFsm.stateMachine(null));
        playStateBasedAction();
    }

    public void selectAction(int p_action)
    {
        m_selectedAction = (Actions)p_action;
    }

    public void sendPlayerActionReq()
    {
        Debug.Log("PState1 PlayerActionReq");
        StartCoroutine(m_playerActionReqHandlerFsm.stateMachine(null));
    }

    public void sendOPlayerReactionResp()
    {
        Debug.Log("OPState1 OPlayerReactionResp");
        if (m_currentMessageObject != null)
            StartCoroutine(m_oplayerReactionReqFsm.stateMachine(m_currentMessageObject));
    }

    public void sendPlayerFinalActionReq()
    {
        Debug.Log("PState4 OPlayerFinalActionReq");
        StartCoroutine(m_playerFinalActionReqHandlerFsm.stateMachine(m_currentMessageObject));
    }

    public void sendPlayerFinalActionResp()
    {
        Debug.Log("OPState5 PlayerFinalActionResp");
        if (m_currentMessageObject != null)
            StartCoroutine(m_oplayerReactionReqFsm.stateMachine(m_currentMessageObject));
        //StartCoroutine(m_playerActionReqHandlerFsm.stateMachine(null));
    }

    public void validateTarget(Players p_targetId)
    {
        if (m_selectedAction.Equals(Actions.ONZ)
            || m_selectedAction.Equals(Actions.Police)
            || m_selectedAction.Equals(Actions.Protest)
            || m_selectedAction.Equals(Actions.Swindle))
        {
            Debug.Log("Targeted player " + p_targetId);
            m_targetedPlayerId = p_targetId;
        }
        else
        {
            m_targetedPlayerId = Players.UNSUPPORTED;
        }
    }




    public void setControlState(bool p_controlEnabled)
    {
        ControlEnabled = p_controlEnabled;
    }
    #endregion

    #region Message_handlers

    private void handleAccesToTurnMasterRights(object p_message)
    {

        CONTROLLER_TurnMasterRightsInd l_controllerTurnMasterRightsInd = (CONTROLLER_TurnMasterRightsInd)p_message;
        Debug.Log("State0 - CONTROLLER_TurnMasterRightsInd acces should be granted for " + l_controllerTurnMasterRightsInd.playerID + "when m_playerId " + m_playerId);
        if (l_controllerTurnMasterRightsInd.playerID.Equals(m_playerId))
        {
            Debug.Log("State0 - CONTROLLER_TurnMasterRightsInd acces granted for " + m_playerId);
            ControlEnabled = true;
            m_hasMasterRights = true;
            ClientPort.markProceeded();
        }
        else
        {
            ControlEnabled = false;
            m_hasMasterRights = false;

        }
        m_currentMessage = Messages.CONTROLLER_TurnMasterRightsInd;
    }

    private void handleReactionReq(object p_message)
    {
        OPLAYER_ReactionReq l_oplayerReactionReq = (OPLAYER_ReactionReq)p_message;
        if (!l_oplayerReactionReq.playerId.Equals(m_playerId))
        {

            Debug.Log(m_playerId + " PState2 OPLAYER_ReactionReq");
            setControlState(true);
            m_oplayerReactionReqFsm = new OPLAYER_ActionReqHandlerFsm(this);
            m_currentMessageObject = l_oplayerReactionReq;
            m_currentMessage = Messages.OPLAYER_ReactionReq;
            Debug.Log("Received " + l_oplayerReactionReq.playedAction + " target: " + l_oplayerReactionReq.targetID);
            PlayerHand l_incomingPlayer = Utils.getPlayer(l_oplayerReactionReq.playerId, m_controller.m_players);
            l_incomingPlayer.PlayedActionsTarget = l_oplayerReactionReq.targetID;
            l_incomingPlayer.PlayedActions = l_oplayerReactionReq.playedAction;
            //StartCoroutine(m_oplayerReactionReqFsm.stateMachine(p_message));
        }
    }

    private void handleDisableReactionReq(object p_message)
    {
        PLAYER_DisableReactionReq l_playerDisableReactionReq = (PLAYER_DisableReactionReq)p_message;
        if (l_playerDisableReactionReq.playerID.Equals(m_playerId))
        {

            Debug.Log(m_playerId + " PState3 PLAYER_DisableReactionReq");

            m_playerDisableReactionReqFsm = new PLAYER_DisableReactionReqFsm(this);
            StartCoroutine(m_playerDisableReactionReqFsm.stateMachine(p_message));
        }
    }

    private void handleOFinalActionReq(object p_message)
    {
        ClientPort.markProceeded();

        OPLAYER_FinalActionReq l_oPlayerFinalActionReq = (OPLAYER_FinalActionReq)p_message;
        OPLAYER_FinalActionResp l_oPlayerFinalActionResp = new OPLAYER_FinalActionResp(l_oPlayerFinalActionReq);
        Debug.Log("Calculating behaviour on CLIENT side");
        ClientPort.send(l_oPlayerFinalActionResp, l_oPlayerFinalActionResp.targetID);
        //StartCoroutine(m_playerDisableReactionReqFsm.stateMachine(p_message));

    }

    private void handleFinalActionResp(object p_message)
    {
        ClientPort.markProceeded();

        PLAYER_FinalActionResp l_playerFinalActionResp = (PLAYER_FinalActionResp)p_message;
        PLAYER_FinalActionAck l_playerFinalActionAck = new PLAYER_FinalActionAck(true);

        Debug.Log("Sending ack");
        ClientPort.send(l_playerFinalActionAck, l_playerFinalActionResp.playerId);
        //StartCoroutine(m_playerDisableReactionReqFsm.stateMachine(p_message));
    }

    private void handleTurnEnd(object p_message)
    {
        CONTROLLER_TurnEnd l_controllerTurnEnd = (CONTROLLER_TurnEnd)p_message;
        if (l_controllerTurnEnd.playerID.Equals(m_playerId))
        {
            ClientPort.markProceeded();
            prepareForNewTurn();
        }
    }

    #endregion

    #region Helper_methods
    private void prepareForNewTurn()
    {
        m_hasMasterRights = false;
        ControlEnabled = false;
    }

    public bool wasTargeted(Players p_player)
    {
        return p_player.Equals(m_playerId);
    }

    #endregion
}