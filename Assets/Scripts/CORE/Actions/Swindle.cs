﻿using Control;
using Enum;
using Message;
using UnityEngine;

[System.Serializable]
public class Swindle : Action
{
    #region Public_Fields

    //public Sprite m_texture;
    //public int m_value;

    #endregion Public_Fields

    public Swindle() : base()
    {
        m_needTarget = true;
    }

    public Swindle(Action p_ocard) : base(p_ocard)
    {
        m_needTarget = true;
    }

    public override void playAction()
    {
        PLAYER_ActionReq l_playerActionReq = new PLAYER_ActionReq(Actions.Swindle);
        Debug.Log("Action " + m_name + " : " + 3 + "millions " + l_playerActionReq.playedAction);
    }

    public override void finalAction(Controller p_controller, PLAYER_ActionReq p_currentPlayerActionReq)
    {
        if (p_controller.m_mainPlayer.Money >= 7)
        {
            p_controller.m_mainPlayer.Money -= 7;
            //Utils.getPlayer(l_controller, l_currentPlayerActionReq.targetID).m_cardsInHand.First.discard();

            PlayerHand l_targetedPlayer = Utils.getPlayer(p_controller, p_currentPlayerActionReq.targetID);
            if (l_targetedPlayer.m_cardsInHand.First != null)
            {
                l_targetedPlayer.m_cardsInHand.First.discard();
                l_targetedPlayer.m_cardsInHand.First = null;
            }
            else
            {
                l_targetedPlayer.m_cardsInHand.Second.discard();
                l_targetedPlayer.m_cardsInHand.Second = null;
            }
        }
    }

    public override void finalActionReverted(Controller p_controller, Players p_player)
    {
        if (p_controller.m_mainPlayer.m_cardsInHand.First != null)
        {
            p_controller.m_mainPlayer.m_cardsInHand.First.discard();
            p_controller.m_mainPlayer.m_cardsInHand.First = null;
        }
        else
        {
            p_controller.m_mainPlayer.m_cardsInHand.Second.discard();
            p_controller.m_mainPlayer.m_cardsInHand.Second = null;
        }
        Utils.getPlayer(p_player, p_controller.m_players).Money -= 7;
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }
}