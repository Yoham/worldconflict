﻿using Control;
using Enum;
using Message;
using UnityEngine;

[System.Serializable]
public class Protest : Card
{
    #region Public_Fields

    //public Sprite m_texture;
    //public int m_value;

    #endregion Public_Fields

    public Protest() : base()
    {
        m_needTarget = true;
    }

    public Protest(Card p_ocard) : base(p_ocard)
    {
        m_needTarget = true;
    }

    public override void playAction()
    {
        PLAYER_ActionReq l_playerActionReq = new PLAYER_ActionReq(Actions.Protest);
        Debug.Log("Action " + m_name + " : " + 3 + "millions " + l_playerActionReq.playedAction);
    }

    public override void finalAction(Controller p_controller, PLAYER_ActionReq p_currentPlayerActionReq)
    {
        if (p_controller.m_mainPlayer.Money >= 3)
        {
            p_controller.m_mainPlayer.Money -= 3;
            PlayerHand l_targetedPlayer = Utils.getPlayer(p_currentPlayerActionReq.targetID, p_controller.m_players);
            if (l_targetedPlayer.m_cardsInHand.First != null)
            {
                l_targetedPlayer.m_cardsInHand.First.discard();
                l_targetedPlayer.m_cardsInHand.First = null;
            }
            else
            {
                if (l_targetedPlayer.m_cardsInHand.Second != null)
                    l_targetedPlayer.m_cardsInHand.Second.discard();
                l_targetedPlayer.m_cardsInHand.Second = null;
            }

        }
    }

    public override void finalActionReverted(Controller p_controller, Players p_player)
    {
        if (p_controller.m_mainPlayer.m_cardsInHand.First != null)
        {
            p_controller.m_mainPlayer.m_cardsInHand.First.discard();
            p_controller.m_mainPlayer.m_cardsInHand.First = null;
        }
        else
        {
            if (p_controller.m_mainPlayer.m_cardsInHand.Second != null)
                p_controller.m_mainPlayer.m_cardsInHand.Second.discard();
            p_controller.m_mainPlayer.m_cardsInHand.Second = null;
        }
        Utils.getPlayer(p_player, p_controller.m_players).Money -= 3;
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }
}