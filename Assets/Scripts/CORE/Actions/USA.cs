﻿using Control;
using Enum;
using Message;
using UnityEngine;

[System.Serializable]
public class USA : Action
{
    #region Public_Fields

    //public Sprite m_texture;
    //public int m_value;

    #endregion Public_Fields

    public USA() : base()
    {
    }

    public USA(Action p_ocard) : base(p_ocard)
    {
    }

    public override void playAction()
    {
        PLAYER_ActionReq l_playerActionReq = new PLAYER_ActionReq(Actions.USA);
        Debug.Log("Action " + m_name + " : " + 3 + "millions " + l_playerActionReq.playedAction);
    }

    public override void finalAction(Controller p_controller, PLAYER_ActionReq p_currentPlayerActionReq)
    {
        p_controller.m_mainPlayer.Money += 1;
    }

    public override void finalActionReverted(Controller p_controller, Players p_player)
    {
        Utils.getPlayer(p_player, p_controller.m_players).Money += 1;
    }
}