﻿using Control;
using Enum;
using Message;
using UnityEngine;

[System.Serializable]
public class Police : Card
{
    #region Public_Fields

    //public Sprite m_texture;
    //public int m_value;

    #endregion Public_Fields

    public Police() : base()
    {
        m_needTarget = true;
    }

    public Police(Card p_ocard) : base(p_ocard)
    {
        m_needTarget = true;
    }

    public override void playAction()
    {
        PLAYER_ActionReq l_playerActionReq = new PLAYER_ActionReq(Actions.Police);
        Debug.Log(l_playerActionReq.ToString());
    }

    public override void finalAction(Controller p_controller, PLAYER_ActionReq p_currentPlayerActionReq)
    {
        p_controller.m_mainPlayer.Money += 2;
        Utils.getPlayer(p_currentPlayerActionReq.targetID, p_controller.m_players).Money -= 2;
    }

    public override void finalActionReverted(Controller p_controller, Players p_player)
    {
        p_controller.m_mainPlayer.Money -= 2;
        Utils.getPlayer(p_player, p_controller.m_players).Money += 2;
    }

}