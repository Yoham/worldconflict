﻿using Enum;
using Message;
using UnityEngine;

[System.Serializable]
public class ONZ : Card
{
    #region Public_Fields

    //public Sprite m_texture;
    //public int m_value;

    #endregion Public_Fields

    public ONZ() : base()
    {
        m_needTarget = true;
    }

    public ONZ(Card p_ocard) : base(p_ocard)
    {
        m_needTarget = true;
    }

    public override void playAction()
    {
        PLAYER_ActionReq l_playerActionReq = new PLAYER_ActionReq(Actions.ONZ);
        Debug.Log("Action " + m_name + " : " + 3 + "millions " + l_playerActionReq.playedAction);
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }
}