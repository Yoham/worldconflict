﻿using Enum;
using Message;
using UnityEngine;

[System.Serializable]
public class UE : Card
{
    #region Public_Fields

    //public Sprite m_texture;
    //public int m_value;

    #endregion Public_Fields

    public UE() : base()
    {
    }

    public UE(Card p_ocard) : base(p_ocard)
    {
    }

    public override void playAction()
    {
        PLAYER_ActionReq l_playerActionReq = new PLAYER_ActionReq(Actions.UE);
        Debug.Log("Action " + m_name + " : " + 3 + "millions " + l_playerActionReq.playedAction);
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }
}