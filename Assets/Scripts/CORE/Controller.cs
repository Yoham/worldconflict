﻿using System.Collections;
using Enum;
using System.Collections.Generic;
using CORE.Server;
using Message;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class Controller : MonoBehaviour
    {
        public BaseFsm m_playerActionReqFsm;
        public BaseFsm m_oPlayerReactionReqFsm;
        public BaseFsm m_playerFinalActionFsm;
        public GameObject m_playerPrefab;
        public CardGenerator m_cardGenerator;
        public PlayerHand m_mainPlayer;
        public List<PlayerHand> m_players;
        public List<PlayerHand> m_oPlayers;
        public bool m_demandNewGame = false;

        public Messages m_currentMessageType = Messages.NONE;

        public Players m_currentPlayerTurn;

        private void Start()
        {
            if (ConnectionHandler.I.m_isHost)
            {
                addPlayerToGame();
                addPlayerToGame();
            }
            else
            {
                addPlayerToGame(Players.Player2);
                addPlayerToGame(Players.Player1);
            }
        }

        public void addPlayerToGame()
        {
            GameObject l_go = Instantiate(m_playerPrefab);
            PlayerHand l_ph = l_go.GetComponent<PlayerHand>();

            m_players.Add(l_ph);
            l_ph.m_playerId = Utils.setPlayerId(m_players);
            l_ph.Money = 2;
            if (l_ph.m_playerId == Players.Player1) m_mainPlayer = l_ph;
            else m_oPlayers.Add(l_ph);
            l_go.name = l_ph.m_playerId.ToString();
        }

        public void addPlayerToGame(Players p_player)
        {
            GameObject l_go = Instantiate(m_playerPrefab);
            PlayerHand l_ph = l_go.GetComponent<PlayerHand>();

            m_players.Add(l_ph);
            l_ph.m_playerId = p_player;
            Utils.setPlayerId(m_players);
            l_ph.Money = 2;
            if (l_ph.m_playerId == Players.Player2) m_mainPlayer = l_ph;
            else m_oPlayers.Add(l_ph);
            l_go.name = l_ph.m_playerId.ToString();
        }

        public void handleMessages(Messages p_messageType, object p_message)
        {
            switch (p_messageType)
            {
                case Messages.PLAYER_ActionReq:
                    Debug.Log("State1 PLAYER_ActionReq");
                    StartCoroutine(m_playerActionReqFsm.stateMachine(p_message));
                    break;

                case Messages.OPLAYER_ReactionResp:
                    Debug.Log("State2 OPLAYER_ReactionResp");

                    StartCoroutine(m_oPlayerReactionReqFsm.stateMachine(p_message));
                    break;

                case Messages.PLAYER_FinalActionReq:
                    Debug.Log("State4 PLAYER_FinalActionReq");
                    ////
                    StartCoroutine(m_playerFinalActionFsm.stateMachine(p_message));
                    break;

                case Messages.OPLAYER_FinalActionResp:
                    Debug.Log("State5 OPLAYER_FinalActionResp");
                    ServerPort.markProceeded();
                    ////
                    OPLAYER_FinalActionResp l_oplayerFinalActionResp = (OPLAYER_FinalActionResp)p_message;
                    PLAYER_FinalActionResp l_playerFinalActionResp = new PLAYER_FinalActionResp(l_oplayerFinalActionResp);
                    //PLAYER_FinalActionAck l_playerFinalActionAck = new PLAYER_FinalActionAck(true);
                    m_mainPlayer.setControlState(false);//should be false
                    ServerPort.send(l_playerFinalActionResp, l_playerFinalActionResp.playerId);
                    break;

                case Messages.PLAYER_FinalActionAck:
                    Debug.Log("State6 PLAYER_FinalActionAck");
                    ServerPort.markProceeded();
                    foreach (var l_ph in m_players)
                    {
                        CONTROLLER_TurnEnd l_controllerTurnEnd = new CONTROLLER_TurnEnd(l_ph.m_playerId);
                        ServerPort.send(l_controllerTurnEnd, l_ph.m_playerId);
                    }
                    BaseFsm.releaseAllFsm();

                    if (m_mainPlayer.m_hasMasterRights)
                    {
                        CONTROLLER_TurnMasterRightsInd l_controllerTurnMasterRightsInd =
                            new CONTROLLER_TurnMasterRightsInd(Players.Player2);
                        ServerPort.send(l_controllerTurnMasterRightsInd, Players.Player2);
                    }
                    else
                    {
                        CONTROLLER_TurnMasterRightsInd l_controllerTurnMasterRightsInd =
                            new CONTROLLER_TurnMasterRightsInd(Players.Player1);
                        ServerPort.send(l_controllerTurnMasterRightsInd, Players.Player1);
                    }

                    break;

                default:
                    break;
            }
        }

        private void Update()
        {
            if (ConnectionHandler.I.m_startNewGame)
            {
                if (m_demandNewGame)
                {
                    if (ConnectionHandler.I.m_isHost)
                    {
                        StartCoroutine(SetUp());
                    }
                }
                if (ConnectionHandler.I.m_isHost)
                {
                    object l_received = ServerPort.receive<object>();
                    if (l_received != null)
                    {
                        //Debug.Log("l_received = " + l_received.GetType() + " \n " + l_received.ToString());
                        Messages l_currentMessageType = Utils.getMessageType(l_received);
                        handleMessages(l_currentMessageType, l_received);

                    }
                }
            }
        }

        private void prepareCards()
        {
            if (m_cardGenerator.m_allCards.Count == 0)
                m_cardGenerator.prepareCards();
        }

        private IEnumerator SetUp()
        {
            if (ConnectionHandler.I.m_isHost)
            {
                prepareCards();
                populatePlayerCards();
                //sendPlayerCards();
                yield return StartCoroutine(sendCards());

                m_demandNewGame = false;
                yield return null;
                m_playerActionReqFsm = new PLAYER_ActionReqFsm(this);

                m_oPlayerReactionReqFsm = new OPLAYER_ReactionReqFsm(this);

                m_playerFinalActionFsm = new OPLAYER_FinalActionReqFsm(this);

                CONTROLLER_TurnMasterRightsInd l_controllerTurnMasterRightsInd =
                    new CONTROLLER_TurnMasterRightsInd(m_mainPlayer.m_playerId);
                ServerPort.send(l_controllerTurnMasterRightsInd, m_mainPlayer.m_playerId);
            }

            yield break;
        }

        private void populatePlayerCards()
        {
            foreach (PlayerHand p_player in m_players)
            {
                if (!p_player.hasCards)
                {
                    p_player.m_cardsInHand = m_cardGenerator.givePlayerCards();
                    p_player.hasCards = true;
                    ConnectionHandler.I.m_cardReceived = true;
                }
            }
        }

        private IEnumerator sendAssignCards(PlayerHand p_who, PlayerHand p_toWho)
        {

            ServerPort.send(new CONTROLLER_AssignId(p_who.m_playerId,
                            p_who.m_cardsInHand.First.m_action, p_who.m_cardsInHand.Second.m_action), p_toWho.m_playerId);
            yield break;
        }

        private IEnumerator sendCards()
        {
            foreach (PlayerHand p_player in m_players)
            {
                if (p_player.m_playerId.Equals(Players.Player2))
                {
                    if (p_player.hasCards)
                    {
                        yield return StartCoroutine(sendAssignCards(p_player, p_player));
                        //ServerPort.send(new CONTROLLER_AssignId(p_player.m_playerId,
                        //    p_player.m_cardsInHand.First.m_action, p_player.m_cardsInHand.Second.m_action), p_player.m_playerId);
                    }
                    yield return null;
                    yield return null;
                    yield return null;
                    if (m_mainPlayer.hasCards)
                    {
                        yield return StartCoroutine(sendAssignCards(m_mainPlayer, p_player));

                        //ServerPort.send(new CONTROLLER_AssignId(m_mainPlayer.m_playerId,
                        //    m_mainPlayer.m_cardsInHand.First.m_action, m_mainPlayer.m_cardsInHand.Second.m_action), p_player.m_playerId);
                    }
                }
            }

            yield break;
        }

        //private void sendPlayerCards()
        //{
        //    foreach (PlayerHand p_player in m_players)
        //    {
        //        if (p_player.m_playerId.Equals(Players.Player2))
        //        {
        //            if (p_player.hasCards)
        //            {
        //                ServerPort.send(new CONTROLLER_AssignId(p_player.m_playerId,
        //                    p_player.m_cardsInHand.First.m_action, p_player.m_cardsInHand.Second.m_action), p_player.m_playerId);
        //            }

        //            if (m_mainPlayer.hasCards)
        //            {
        //                ServerPort.send(new CONTROLLER_AssignId(m_mainPlayer.m_playerId,
        //                    m_mainPlayer.m_cardsInHand.First.m_action, m_mainPlayer.m_cardsInHand.Second.m_action), p_player.m_playerId);
        //            }
        //        }
        //    }
        //}

        public virtual void playAction()
        {
            if (!m_mainPlayer.m_currentMessage.Equals(Messages.OPLAYER_ReactionReq) &&
                !m_mainPlayer.m_currentMessage.Equals(Messages.PLAYER_ActionResp))
            {
                m_mainPlayer.playAction();
            }

        }

        public virtual void playReaction()
        {
            if (m_mainPlayer.m_currentMessage.Equals(Messages.OPLAYER_ReactionReq) ||
                m_mainPlayer.m_currentMessage.Equals(Messages.PLAYER_ActionResp))
            {
                m_mainPlayer.playAction();
            }
            //m_mainPlayer.playAction();
        }

        public void selectAction(int p_action)
        {
            m_mainPlayer.m_selectedAction = (Actions)p_action;
        }

        public void selectReaction(int p_reaction)
        {
            m_mainPlayer.m_selectedReaction = (Reactions)p_reaction;
            playReaction();
        }

        public Card getCardType(Actions p_actions)
        {
            switch (p_actions)
            {
                case Actions.Police:
                    return m_cardGenerator.m_police;

                case Actions.Russia:
                    return m_cardGenerator.m_russia;

                case Actions.Protest:
                    return m_cardGenerator.m_protest;

                case Actions.UE:
                    return m_cardGenerator.m_ue;

                case Actions.Media:
                    return m_cardGenerator.m_media;

                case Actions.ONZ:
                    return m_cardGenerator.m_onz;

                default:
                    return null;
            }
        }
    }
}