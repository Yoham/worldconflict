﻿using Enum;
using Message;
using System.Collections;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class OPLAYER_ActionReqHandlerFsm : UFSM
    {
        private OPLAYER_ReactionReq m_currentPlayerActionReq;
        private OPLAYER_ReactionResp m_currentPlayerActionResp;

        public OPLAYER_ActionReqHandlerFsm(PlayerHand p_player) : base()
        {
            m_player = p_player;
            m_mono = p_player;
            m_controller = p_player.m_controller;
        }

        public override IEnumerator stateMachine(object p_message)
        {
            markReceived();
            if (State.Equals(States.Idle))
            {
                handleStateIdle(p_message);
            }

            yield break;
        }

        protected override void handleStateIdle(object p_message)
        {
            OPLAYER_ReactionReq l_oplayerReactionReq = (OPLAYER_ReactionReq)p_message;
            m_player.setControlState(true);
            m_player.m_currentMessage = Messages.OPLAYER_ReactionReq;
            m_currentPlayerActionReq = l_oplayerReactionReq;
            prepareReactionResponse(l_oplayerReactionReq);
        }

        private void prepareReactionResponse(OPLAYER_ReactionReq p_oplayerReactionReq)
        {
            OPLAYER_ReactionResp l_oplayerReactionResp = new OPLAYER_ReactionResp(p_oplayerReactionReq)
            {
                playerId = m_player.m_playerId,
                oPlayerReaction = m_player.m_selectedReaction,
                responseAction = m_player.m_selectedAction,
                wasTargeted  = m_player.wasTargeted(p_oplayerReactionReq.targetID)
            };
            m_currentPlayerActionResp = l_oplayerReactionResp;
            if (m_player.m_selectedReaction.Equals(Reactions.Pass))
            {
                Utils.getCardType(p_oplayerReactionReq.playedAction)
                    .finalActionReverted(m_controller, p_oplayerReactionReq.playerId);
            }

            sendMessage<OPLAYER_ReactionResp>(l_oplayerReactionResp, m_player.m_playerId);
            stateTransition(States.WaitingForResponse);
        }

    }
}