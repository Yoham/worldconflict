﻿using Enum;
using Message;
using System.Collections;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class PLAYER_DisableReactionReqFsm : UFSM
    {
        private OPLAYER_ReactionReq m_currentPlayerActionReq;
        private OPLAYER_ReactionResp m_currentPlayerActionResp;

        public PLAYER_DisableReactionReqFsm(PlayerHand p_player) : base()
        {
            m_player = p_player;
            m_mono = p_player;
            m_controller = p_player.m_controller;
        }

        public override IEnumerator stateMachine(object p_message)
        {
            markReceived();
            if (State.Equals(States.Idle))
            {
                handleStateIdle(p_message);
            }

            yield break;
        }

        protected override void handleStateIdle(object p_message)
        {
            PLAYER_DisableReactionReq l_playerDisableReactionReq = (PLAYER_DisableReactionReq)p_message;
            PLAYER_DisableReactionResp l_playerDisableReactionResp = new PLAYER_DisableReactionResp(l_playerDisableReactionReq);
            l_playerDisableReactionResp.playerID = m_player.m_playerId;
            m_player.setControlState(false);

            sendMessage<PLAYER_DisableReactionResp>(l_playerDisableReactionResp, m_player.m_playerId);
            stateTransition(States.Idle);

        }

    }
}