﻿using System;
using Enum;
using Message;
using System.Collections;
using UnityEngine;

namespace Control
{
	[System.Serializable]
	public class PLAYER_FinalActionReqHandlerFsm : UFSM
	{
		private PLAYER_FinalActionReq m_currentPlayerFinalActionReq;
		private PLAYER_FinalActionResp m_currentPlayerFinalActionResp;

		public PLAYER_FinalActionReqHandlerFsm(PlayerHand p_player) : base()
		{
			m_player = p_player;
			m_mono = p_player;
			m_controller = p_player.m_controller;
		}

		public override IEnumerator stateMachine(object p_message)
		{
			if (State.Equals(States.Idle))
			{
				handleStateIdle(p_message);
			}
			if (State.Equals(States.WaitingForResponse))
			{
				while (true)
				{
					m_coroutine = new FsmCoroutine(m_mono, getMessage<PLAYER_FinalActionReq>());
					yield return m_coroutine.coroutine;
					if (!(m_coroutine.result is PLAYER_FinalActionReq)) continue;
					handleStateWaitingForResponse(m_coroutine.result);
					break;
				}
			}
		}

		protected override void handleStateIdle(object p_message)
		{
			PLAYER_FinalActionReq l_playerFinalActionReq = (PLAYER_FinalActionReq) p_message;
			m_currentPlayerFinalActionReq = l_playerFinalActionReq;
			m_player.setControlState(false);
			sendMessage<PLAYER_FinalActionReq>(l_playerFinalActionReq, l_playerFinalActionReq.playerId);
			stateTransition(States.WaitingForResponse);
		}

		protected override void handleStateWaitingForResponse(object p_message)
		{
			PLAYER_FinalActionResp l_playerFinalActionResp = (PLAYER_FinalActionResp)p_message;
            PLAYER_FinalActionAck l_playerFinalActionAck = new PLAYER_FinalActionAck(true);
            if (!l_playerFinalActionResp.playerReaction.Equals(Reactions.Pass))
			{
				//PLAYER_FinalActionReq l_playerFinalActionReq = new PLAYER_FinalActionReq();
				m_player.setControlState(true);
				m_player.m_currentMessage = Messages.PLAYER_ActionResp;
                sendMessage<PLAYER_FinalActionAck>(l_playerFinalActionAck, m_player.m_playerId);
                //sendMessage<PLAYER_FinalActionReq>(l_playerFinalActionReq);
            }
			else
			{
				m_player.setControlState(true);//should be false
				sendMessage<PLAYER_FinalActionAck>(l_playerFinalActionAck, m_player.m_playerId);

				//Utils.getActionType(m_currentPlayerFinalActionReq.playedAction).finalAction(m_controller, m_currentPlayerFinalActionReq);
			}

			stateTransition(States.Idle);
		}
	}
}