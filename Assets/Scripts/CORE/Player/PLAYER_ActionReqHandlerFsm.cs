﻿using System;
using Enum;
using Message;
using System.Collections;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class PLAYER_ActionReqHandlerFsm : UFSM
    {
        private PLAYER_ActionReq m_currentPlayerActionReq;
        private PLAYER_ActionResp m_currentPlayerActionResp;

        public PLAYER_ActionReqHandlerFsm(PlayerHand p_player) : base()
        {
            m_player = p_player;
            m_mono = p_player;
            m_controller = p_player.m_controller;
        }

        public override IEnumerator stateMachine(object p_message)
        {
            if (State.Equals(States.Idle))
            {
                handleStateIdle(p_message);
            }
            if (State.Equals(States.WaitingForResponse))
            {
                while (true)
                {
                    m_coroutine = new FsmCoroutine(m_mono, getMessage<PLAYER_ActionResp>());
                    yield return m_coroutine.coroutine;
                    if (!(m_coroutine.result is PLAYER_ActionResp)) continue;
                    handleStateWaitingForResponse(m_coroutine.result);
                    break;
                }
            }
        }

        protected override void handleStateIdle(object p_message)
        {
            PLAYER_ActionReq l_playerActionReq = new PLAYER_ActionReq(m_player);
            m_currentPlayerActionReq = l_playerActionReq;
            m_player.setControlState(false);
            sendMessage<PLAYER_ActionReq>(l_playerActionReq, m_player.m_playerId);
            stateTransition(States.WaitingForResponse);
        }

        protected override void handleStateWaitingForResponse(object p_message)
        {
            PLAYER_ActionResp l_playerActionResp = (PLAYER_ActionResp)p_message;

            if (!l_playerActionResp.playerReaction.Equals(Reactions.Pass))
            {
                PLAYER_FinalActionReq l_playerFinalActionReq = new PLAYER_FinalActionReq(l_playerActionResp)
                {
                    playerReaction = m_player.m_selectedReaction
                };

                m_player.setControlState(true);
                m_player.m_currentMessage = Messages.PLAYER_ActionResp;
                m_player.m_currentMessageObject = l_playerFinalActionReq;
                Debug.Log("Received " + l_playerActionResp.responseAction + " target: " + l_playerActionResp.targetID);
                PlayerHand l_incomingPlayer = Utils.getPlayer(l_playerActionResp.targetID, m_controller.m_players);
                l_incomingPlayer.PlayedActionsTarget = l_playerActionResp.playerId;
                l_incomingPlayer.PlayedActions = l_playerActionResp.responseAction;
                //sendMessage<PLAYER_FinalActionReq>(l_playerFinalActionReq, l_playerFinalActionReq.targetID);
            }
            else
            {
                PLAYER_FinalActionAck l_playerFinalActionAck = new PLAYER_FinalActionAck(true);
                m_player.setControlState(true);//should be false
                sendMessage<PLAYER_FinalActionAck>(l_playerFinalActionAck, m_player.m_playerId);

                Utils.getActionType(m_currentPlayerActionReq.playedAction).finalAction(m_controller, m_currentPlayerActionReq);
            }

            stateTransition(States.Idle);
        }
    }
}