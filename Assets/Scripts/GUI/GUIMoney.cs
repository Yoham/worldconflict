﻿using Holoville.HOTween;
using UnityEngine;
using UnityEngine.UI;

public class GUIMoney : MonoBehaviour
{
    public Image m_image;
    public RectTransform m_coin;
    private RectTransform m_myTransform;
    private PlayerHand m_player;

    private void Start()
    {
        m_myTransform = GetComponent<RectTransform>();
    }

    public void setMoney(int p_money)
    {
        HOTween.To(m_coin, 1, "anchoredPosition", positions[p_money]);
    }

    private void Update()
    {
        //Debug.Log(m_coin.anchoredPosition);
        //m_coin.anchoredPosition += new Vector2(10,0);//new Vector2(Input.mousePosition.x,Input.mousePosition.y);

        //Debug.Log(m_myTransform.rect);
        //step();
    }

    private Vector2[] positions =
    {
        new Vector2(0, 0) ,
        new Vector2(210, 0),
        new Vector2(420, 0),
        new Vector2(0, -210),
        new Vector2(210, -210),
        new Vector2(420, -210),
        new Vector2(0, 0),
        new Vector2(210, 0),
        new Vector2(420, 0),
        new Vector2(0, -210),
        new Vector2(210, -210),
        new Vector2(420, -210)
    };
}