﻿using Enum;
using UnityEngine;

public class GUIAction : MonoBehaviour
{
    public void playAction(int p_actionEnum)
    {
        Utils.getActionType((Actions)p_actionEnum).CmdPlayAction();
    }
}