﻿using Control;
using Enum;
using System.Collections;
using System.Collections.Generic;
using CORE.Server;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    #region Singleton

    private static GUIController _instance;

    static public bool isActive
    {
        get
        {
            return _instance != null;
        }
    }

    static public GUIController I
    {
        get
        {
            if (_instance == null)
            {
                _instance = Object.FindObjectOfType(typeof(GUIController)) as GUIController;

                if (_instance == null)
                {
                    GameObject go = new GameObject("_gamemanager");
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<GUIController>();
                }
            }
            return _instance;
        }
    }

    #endregion Singleton

    public GUIPlayer m_player;
    public Controller m_controller;
    public RectTransform m_oPlayersPanel;
    public GameObject m_oPlayerPrefab;
    public List<GUIPlayer> m_oPlayers;
    public Sprite m_discardSprite;
    public Button m_protestButton;
    public Button m_swindleButton;
    public bool m_isControlEnabled;

    private bool m_finished = false;

    public List<Button> m_allButtons;

    public List<Button> m_playableButton;

    private void Start()
    {
        //StartCoroutine(Load());
    }

    private void Update()
    {

        if (ConnectionHandler.I.m_startNewGame)
        {
            if (!m_finished)
            {
                m_finished = true;
                StartCoroutine(Load());
            }

        }
    }

    public IEnumerator Load()
    {
        yield return null;
        m_controller.m_mainPlayer.OnMoneyChanged += disableControls;
        populateOPlayers();
        getAllButtons();
        yield break;
    }

    private void populateOPlayers()
    {
        foreach (PlayerHand l_ph in m_controller.m_oPlayers)
        {
            GameObject l_go = Instantiate(m_oPlayerPrefab);

            l_go.transform.SetParent(m_oPlayersPanel.transform);
            l_go.GetComponent<RectTransform>().localScale = Vector3.one;
            l_go.name = l_ph.m_playerId.ToString();
            l_go.GetComponent<GUIOPlayer>().setData(l_ph);
        }
    }

    public void disableControls()
    {
        disableControls(true);
    }

    public void disableControls(bool p_controlEnabled)
    {
        if (m_player.m_player.Money >= 10)
        {
            disableAllButtons();
            setButtonInteractivity(m_swindleButton, true);
            enableReactionButtons();
        }
        else
        {
            setCardButtonActivity(p_controlEnabled);
            setProtestButtonActivity(p_controlEnabled);
            setSwindleButtonActivity(p_controlEnabled);
        }
    }

    private void setSwindleButtonActivity(bool p_controlEnabled)
    {
        if (p_controlEnabled)
        {
            if (m_player.m_player.Money < 7)
            {
                setButtonInteractivity(m_swindleButton, false);
            }
            else
            {
                setButtonInteractivity(m_swindleButton, true);
            }
        }


    }

    private void setProtestButtonActivity(bool p_controlEnabled)
    {
        if (p_controlEnabled)
        {
            if (m_player.m_player.Money < 3)
            {
                setButtonInteractivity(m_protestButton, false);
            }
            else
            {
                setButtonInteractivity(m_protestButton, true);
            }
        }


    }

    private void getAllButtons()
    {
        foreach (Button l_button in m_player.GetComponentsInChildren(typeof(Button)))
        {

            m_allButtons.Add(l_button);
        }
    }

    private void setCardButtonActivity(bool p_controlEnabled)
    {
        if (p_controlEnabled)
        {

            if (m_player.m_cards1.m_actions.Equals(Actions.Protest))
            {
                if (m_player.m_player.Money < 3)
                {
                    setButtonInteractivity(m_player.m_cards1.m_button, false);
                }
            }
            else
            {
                setButtonInteractivity(m_player.m_cards1.m_button, true);
            }

            if (m_player.m_cards2.m_actions.Equals(Actions.Protest))
            {
                if (m_player.m_player.Money < 3)
                {
                    setButtonInteractivity(m_player.m_cards2.m_button, false);
                }
            }
            else
            {
                setButtonInteractivity(m_player.m_cards2.m_button, true);
            }
        }


    }

    private void setButtonInteractivity(Button p_button, bool p_state)
    {
        p_button.interactable = p_state;
    }

    public void switchAllButtons(bool p_state)
    {
        if (p_state) enableAllButtons();
        else disableAllButtons();
    }

    private void disableAllButtons()
    {
        foreach (Button l_button in m_allButtons)
        {
            setButtonInteractivity(l_button, false);
        }
    }

    private void enableAllButtons()
    {
        foreach (Button l_button in m_allButtons)
        {
            setButtonInteractivity(l_button, true);
        }
        //disableControls(true);
    }

    public void disableReactionButtons()
    {
        foreach (Button l_button in m_playableButton)
        {
            if (l_button.name.Equals("PlayCard"))
            {
                setButtonInteractivity(l_button, true);
            }
            else
            {
                setButtonInteractivity(l_button, false);

            }
        }
    }

    public void enableReactionButtons()
    {
        foreach (Button l_button in m_playableButton)
        {
            setButtonInteractivity(l_button, true);

        }
    }
}