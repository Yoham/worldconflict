﻿using Control;
using System.Collections;
using CORE.Server;
using UnityEngine;
using UnityEngine.UI;

public class GUIPlayer : MonoBehaviour
{
    public GUIMoney m_money;
    public GUICard m_cards1;
    public GUICard m_cards2;
    public GameObject m_actions;
    public PlayerHand m_player;

    private Image card_1;
    private Image card_2;
    private bool m_finished = false;

    private void Start()
    {
        //StartCoroutine(Load());
    }

    private IEnumerator Load()
    {
        yield return null;
        m_player = GameObject.FindObjectOfType<Controller>().m_mainPlayer;
        m_cards1.setCard(m_player.m_cardsInHand.First);
        m_cards2.setCard(m_player.m_cardsInHand.Second);
        m_player.OnMoneyChanged += refreshMoney;
        m_player.OnControlStateChanged += GUIController.I.switchAllButtons;

        yield break;
    }

    private void Update()
    {
        if (ConnectionHandler.I.m_startNewGame)
        {
            if (ConnectionHandler.I.m_cardReceived)
            {
                if (!m_finished)
                {
                    m_finished = true;
                    StartCoroutine(Load());
                }
            }
        }
    }

    public void refreshMoney()
    {
        m_money.setMoney(m_player.Money);
    }

}