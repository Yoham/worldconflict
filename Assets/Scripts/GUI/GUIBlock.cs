﻿using Enum;
using UnityEngine;

public class GUIBlock : MonoBehaviour
{
    public GameObject m_container;


    public void showBlockPanel()
    {
        m_container.changeActive();
    }

    public void playBlock(int p_action)
    {
        GUIController.I.m_controller.m_mainPlayer.m_selectedAction = (Actions)p_action;
        GUIController.I.m_controller.m_mainPlayer.m_selectedReaction = Reactions.Block;
        if (GUIController.I.m_controller.m_mainPlayer.m_currentMessage.Equals(Messages.OPLAYER_ReactionReq))
        {
            GUIController.I.m_controller.m_mainPlayer.playAction();
            m_container.changeActive();
        }
       
    }


}