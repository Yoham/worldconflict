﻿using Enum;
using UnityEngine;
using UnityEngine.UI;

public class GUIOPlayer : MonoBehaviour
{
    public Text m_playerName;
    public Players m_playerId;
    public GameObject m_actions;
    public PlayerHand m_player;
    public Text m_money;

    public Image card_1;
    public Image card_2;
    public Image m_played;
    public Text m_playedTarget;

    public void setData(PlayerHand p_player)
    {
        m_player = p_player;
        m_playerId = p_player.m_playerId;
        m_money.text = p_player.Money.ToString();
        m_playerName.text = p_player.m_playerId.ToString();
        setCards(p_player.m_cardsInHand);
        m_player.OnMoneyChanged += refreshMoney;
        m_player.OnPlayedCardChanged += setPlayedAction;
        m_player.m_cardsInHand.OnCardChanged += setCards;
        m_player.m_cardsInHand.First.OnCardChanged += disable1;
        m_player.m_cardsInHand.Second.OnCardChanged += disable2;
    }

    public void setCards(Cards p_cards)
    {
        card_1.sprite = p_cards.First != null ? p_cards.First.m_texture : GUIController.I.m_discardSprite;
        card_2.sprite = p_cards.Second != null ? p_cards.Second.m_texture : GUIController.I.m_discardSprite;
    }

    public void disable1(Card p_card)
    {
        card_1.gameObject.SetActive(false);
    }

    public void disable2(Card p_card)
    {
        card_2.gameObject.SetActive(false);
    }

    public void refreshMoney()
    {
        m_money.text = m_player.Money.ToString();
    }

    public void selectAsTarget()
    {
        GUIController.I.m_controller.m_mainPlayer.validateTarget(m_playerId);
    }

    public void setPlayedAction()
    {
        m_played.gameObject.SetActive(true);
        m_played.sprite = m_player.m_controller.m_cardGenerator.getCardSprite(m_player.PlayedActions);
        m_playedTarget.text = m_player.PlayedActionsTarget.ToString();
    }
}