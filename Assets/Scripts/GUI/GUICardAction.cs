﻿using Enum;
using UnityEngine;

public class GUICardAction : MonoBehaviour
{
    public delegate void ClickAction(Actions p_actions);

    public static event ClickAction OnClicked;

    public void playAction(int p_actionEnum)
    {
        Utils.getCardType((Actions)p_actionEnum).CmdPlayAction();
    }

    public void playAction(Actions p_actionEnum)
    {
        Utils.getCardType(p_actionEnum).CmdPlayAction();
    }

    public void OnClick(int p_actionEnum)
    {
        OnClicked += playAction;
        if (OnClicked != null)
            OnClicked((Actions)p_actionEnum);

        OnClicked -= playAction;
    }
}