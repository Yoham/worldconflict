﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIDiscarded : MonoBehaviour
{
    public Image[] m_cardImages;
    public Text[] m_cardText;
    public Sprite m_discardSprite;

    private void Start()
    {
        GUIController.I.m_controller.m_cardGenerator.OnDiscardedChanged += getDiscardedData;
    }

    private void Update()
    {
    }

    public void getDiscardedData(Dictionary<Card, int> p_discarded)
    {
        int l_it = 0;
        foreach (KeyValuePair<Card, int> p_card in p_discarded)
        {
            m_cardImages[l_it].sprite = p_card.Key.m_texture;
            m_cardText[l_it].text = p_card.Value.ToString();
            l_it++;
        }
    }
}