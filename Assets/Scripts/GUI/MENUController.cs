﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using CORE.Server;
using UnityEngine.SceneManagement;


public class MENUController : MonoBehaviour
{
    #region Singleton

    private static MENUController _instance;

    public static bool isActive
    {
        get { return _instance != null; }
    }

    public static MENUController I
    {
        get
        {
            if (_instance == null)
            {
                _instance = UnityEngine.Object.FindObjectOfType(typeof(MENUController)) as MENUController;

                if (_instance == null)
                {
                    GameObject go = new GameObject("_gamemanager");
                    DontDestroyOnLoad(go);
                    _instance = go.AddComponent<MENUController>();
                }
            }
            return _instance;
        }
    }

    #endregion Singleton

    public Button m_hostButton;
    public Button m_joinButton;
    public Button m_rulesButton;
    public Dropdown m_dropdown;
    public InputField m_ipInputField;

    public void StartServer()
    {
        ConnectionHandler.I.server.startServ();
        ConnectionHandler.I.m_isHost = true;
        SceneManager.LoadScene("_Main");
    }

    public void ConnectToServer()
    {
        ConnectionHandler.I.m_client.ConnectToServer(m_ipInputField.text);
        ConnectionHandler.I.m_isHost = false;
        SceneManager.LoadScene("_Main");
        //SceneManager.LoadScene("_Menu2");
    }

    public void sendCrap()
    {
        ConnectionHandler.I.m_client.send("crap");
    }

    public void sendToClient()
    {
        ConnectionHandler.I.m_server.send("Message from server to you!");
    }

    public void sendToAllClient()
    {
        ConnectionHandler.I.m_server.sendAll();
    }

    public void stopServer()
    {
        Application.Quit();
        ConnectionHandler.I.m_server.OnApplicationQuit();
        ConnectionHandler.I.m_client.OnApplicationQuit();
    }

    public void setMaxPlayer()
    {
        ConnectionHandler.I.m_server.maxPlayers = m_dropdown.value + 1;
        Debug.Log("Max players set to: " + ConnectionHandler.I.m_server.maxPlayers.ToString());
    }
}