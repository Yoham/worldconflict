﻿using Control;
using Enum;
using UnityEngine;
using UnityEngine.UI;

public class GUICard : MonoBehaviour
{
    public Image m_image;
    public Actions m_actions;
    private Action m_action;
    public Button m_button;
    private Card m_card;

    public void setCard(Card p_card)
    {
        m_card = p_card;
        m_action = p_card;
        m_image.sprite = p_card.m_texture;
        m_actions = p_card.m_action;
        m_card.OnCardChanged += removeCard;
    }

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void playAction()
    {
        selectAction();
    }

    public void removeCard(Card p_cards)
    {
        this.gameObject.SetActive(false);
    }

    public void selectAction()
    {
        GameObject.FindObjectOfType<Controller>().m_players[0].m_selectedAction = m_actions;
    }
}