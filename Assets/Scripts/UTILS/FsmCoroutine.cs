﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class FsmCoroutine
{
    public Coroutine coroutine { get; private set; }
    public object result;
    private IEnumerator target;

    public FsmCoroutine(MonoBehaviour owner, IEnumerator target)
    {
        this.target = target;
        this.coroutine = owner.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        while (target.MoveNext())
        {
            result = target.Current;
            yield return result;
        }
    }
};

//FsmCoroutine cdd = new FsmCoroutine(m_mono, a(handleStateWaitingForResponse));
//yield return cdd.coroutine;
//public delegate void state(object p_obj);



//public IEnumerator a(state a)
//{
//    yield return a;
//    yield break;
//}
