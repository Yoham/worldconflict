﻿using System;
using System.Collections;
using Enum;
using UnityEngine;

namespace Control
{
    [Serializable]
    public class UFSM : BaseFsm
    {
        protected PlayerHand m_player;

        protected UFSM() : base()
        {
        }

        protected override void sendMessage<T>(T p_messageType, Players p_player)
        {
            Debug.Log("Sending message " + typeof(T) +" " + p_messageType.ToString());
            ClientPort.send(p_messageType, p_player);
        }

        protected override IEnumerator getMessage<T>()
        {
            object l_received = ClientPort.receive<object>();
            yield return l_received;
            if (l_received != null)
            {
                if (l_received.GetType() == typeof (T))
                {
                    markReceived();
                }
            }
            yield break;
        }

        protected override void markReceived()
        {
            ClientPort.markProceeded();
        }
    }
}