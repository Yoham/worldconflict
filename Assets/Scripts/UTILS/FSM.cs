﻿using Enum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class BaseFsm
    {
        public static List<BaseFsm> s_allFsm= new List<BaseFsm>(); 
        protected MonoBehaviour m_mono;
        protected Controller m_controller;
        protected FsmCoroutine m_coroutine;
        private States m_state = States.Idle;
        private States m_nextState;

        protected bool locked = false;

        public States State
        {
            get { return m_state; }
            set
            {
                m_state = value;
            }
        }

        protected BaseFsm()
        {
            s_allFsm.Add(this);
        }

        protected virtual void stateTransition(States p_state)
        {
            State = p_state;
        }

        protected virtual void handleStateIdle(object p_message)
        {
        }

        protected virtual void handleStateWaitingForResponse(object p_message)
        {
        }

        protected virtual void handleStateProceedingResponse(object p_message)
        {
        }

        protected virtual void validateMessage(object p_message)
        {
        }

        protected virtual void sendMessage<T>(T p_messageType, Players p_player)
        {
        }
        
        public virtual IEnumerator stateMachine(object p_message)
        {
            yield break;
        }

        //TODO
        protected virtual IEnumerator getMessage<T>()
        {
            yield return new WaitForSeconds(2f);
            Debug.Log("2s Base getMessage()");
            yield break;
        }

        protected virtual void markReceived()
        {
        }

        public static void releaseAllFsm()
        {
            foreach (BaseFsm p_baseFsm in s_allFsm)
            {
                p_baseFsm.State=States.Idle;
                p_baseFsm.locked = false;
            }
        }
    }
}