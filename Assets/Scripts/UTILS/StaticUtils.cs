﻿using Control;
using Enum;
using System.Collections.Generic;
using Message;
using Newtonsoft.Json;
using UnityEngine;

[System.Serializable]
public static class Utils
{
    public static void changeActive(this GameObject p_go)
    {
        p_go.SetActive(!p_go.activeSelf);
    }

    public static Card getCardType(Actions p_action)
    {
        switch (p_action)
        {
            case Actions.Police:
                return new Police();

            case Actions.Russia:
                return new Russia();

            case Actions.Protest:
                return new Protest();

            case Actions.UE:
                return new UE();

            case Actions.Media:
                return new Media();

            case Actions.ONZ:
                return new ONZ();

            default:
                return null;
        }
    }

    public static Card getPlayerCard(Cards p_cards, Actions p_action)
    {
        return p_cards.First.m_action == p_action
            ? p_cards.First
            : (p_cards.Second.m_action == p_action ? p_cards.Second : null);
    }

    public static Action getActionType(Actions p_action)
    {
        switch (p_action)
        {
            case Actions.USA:
                return new USA();

            case Actions.LocalBuisiness:
                return new LocalBuisiness();

            case Actions.Swindle:
                return new Swindle();

            case Actions.Police:
                return new Police();

            case Actions.Russia:
                return new Russia();

            case Actions.Protest:
                return new Protest();

            case Actions.UE:
                return new UE();

            case Actions.Media:
                return new Media();

            case Actions.ONZ:
                return new ONZ();

            default:
                return null;
        }
    }

    //public static void enqueueOnPort(object p_obj)
    //{
    //    if (p_obj is PLAYER_ActionReq) SendToUserPort.enqueueMessage(p_obj);
    //    if (p_obj is PLAYER_ActionResp) SendToControlPort.enqueueMessage(p_obj);
    //    if (p_obj is OPLAYER_ReactionReq) SendToControlPort.enqueueMessage(p_obj);
    //    if (p_obj is OPLAYER_ReactionResp) SendToUserPort.enqueueMessage(p_obj);
    //    if (p_obj is CONTROLLER_TurnEnd) SendToControlPort.enqueueMessage(p_obj);
    //    if (p_obj is CONTROLLER_TurnMasterRightsInd) SendToControlPort.enqueueMessage(p_obj);
    //    if (p_obj is PLAYER_DisableReactionReq) SendToControlPort.enqueueMessage(p_obj);
    //    if (p_obj is PLAYER_DisableReactionResp) SendToUserPort.enqueueMessage(p_obj);
    //    if (p_obj is OPLAYER_FinalActionReq) SendToControlPort.enqueueMessage(p_obj);
    //    if (p_obj is OPLAYER_FinalActionResp) SendToUserPort.enqueueMessage(p_obj);
    //    if (p_obj is PLAYER_FinalActionAck) SendToUserPort.enqueueMessage(p_obj);
    //    if (p_obj is PLAYER_FinalActionReq) SendToUserPort.enqueueMessage(p_obj);
    //    if (p_obj is PLAYER_FinalActionResp) SendToControlPort.enqueueMessage(p_obj);
    //}

    public static Messages getMessageType(object p_obj)
    {
        if (p_obj is PLAYER_ActionReq) return Messages.PLAYER_ActionReq;
        if (p_obj is PLAYER_ActionResp) return Messages.PLAYER_ActionResp;
        if (p_obj is OPLAYER_ReactionReq) return Messages.OPLAYER_ReactionReq;
        if (p_obj is OPLAYER_ReactionResp) return Messages.OPLAYER_ReactionResp;
		if (p_obj is CONTROLLER_AssignId) return Messages.CONTROLLER_AssignId;
		if (p_obj is CONTROLLER_TurnEnd) return Messages.CONTROLLER_TurnEnd;
        if (p_obj is CONTROLLER_TurnMasterRightsInd) return Messages.CONTROLLER_TurnMasterRightsInd;
        if (p_obj is PLAYER_DisableReactionReq) return Messages.PLAYER_DisableReactionReq;
        if (p_obj is PLAYER_DisableReactionResp) return Messages.PLAYER_DisableReactionResp;
        if (p_obj is OPLAYER_FinalActionReq) return Messages.OPLAYER_FinalActionReq;
        if (p_obj is OPLAYER_FinalActionResp) return Messages.OPLAYER_FinalActionResp;
        if (p_obj is PLAYER_FinalActionAck) return Messages.PLAYER_FinalActionAck;
        if (p_obj is PLAYER_FinalActionReq) return Messages.PLAYER_FinalActionReq;
        if (p_obj is PLAYER_FinalActionResp) return Messages.PLAYER_FinalActionResp;

        return Messages.NONE;
    }

    public static object deserializeByType(Messages p_type, string p_toDeserialize)
    {
        if (p_type.Equals(Messages.PLAYER_ActionReq)) return JsonConvert.DeserializeObject<PLAYER_ActionReq>(p_toDeserialize);
        if (p_type.Equals(Messages.PLAYER_ActionResp)) return JsonConvert.DeserializeObject<PLAYER_ActionResp>(p_toDeserialize);
        if (p_type.Equals(Messages.OPLAYER_ReactionReq)) return JsonConvert.DeserializeObject<OPLAYER_ReactionReq>(p_toDeserialize);
		if (p_type.Equals(Messages.OPLAYER_ReactionResp)) return JsonConvert.DeserializeObject<OPLAYER_ReactionResp>(p_toDeserialize);
		if (p_type.Equals(Messages.CONTROLLER_AssignId)) return JsonConvert.DeserializeObject<CONTROLLER_AssignId>(p_toDeserialize);
        if (p_type.Equals(Messages.CONTROLLER_TurnEnd)) return JsonConvert.DeserializeObject<CONTROLLER_TurnEnd>(p_toDeserialize);
        if (p_type.Equals(Messages.CONTROLLER_TurnMasterRightsInd)) return JsonConvert.DeserializeObject<CONTROLLER_TurnMasterRightsInd>(p_toDeserialize);
        if (p_type.Equals(Messages.PLAYER_DisableReactionReq)) return JsonConvert.DeserializeObject<PLAYER_DisableReactionReq>(p_toDeserialize);
        if (p_type.Equals(Messages.PLAYER_DisableReactionResp)) return JsonConvert.DeserializeObject<PLAYER_DisableReactionResp>(p_toDeserialize);
        if (p_type.Equals(Messages.OPLAYER_FinalActionReq)) return JsonConvert.DeserializeObject<OPLAYER_FinalActionReq>(p_toDeserialize);
        if (p_type.Equals(Messages.OPLAYER_FinalActionResp)) return JsonConvert.DeserializeObject<OPLAYER_FinalActionResp>(p_toDeserialize);
        if (p_type.Equals(Messages.PLAYER_FinalActionAck)) return JsonConvert.DeserializeObject<PLAYER_FinalActionAck>(p_toDeserialize);
        if (p_type.Equals(Messages.PLAYER_FinalActionReq)) return JsonConvert.DeserializeObject<PLAYER_FinalActionReq>(p_toDeserialize);
        if (p_type.Equals(Messages.PLAYER_FinalActionResp)) return JsonConvert.DeserializeObject<PLAYER_FinalActionResp>(p_toDeserialize);

        return null;
    }

    public static Messages getMessageType(string p_type)
    {
        if (p_type.Equals("Message.PLAYER_ActionReq")) return Messages.PLAYER_ActionReq;
        if (p_type.Equals("Message.PLAYER_ActionResp")) return Messages.PLAYER_ActionResp;
        if (p_type.Equals("Message.OPLAYER_ReactionReq")) return Messages.OPLAYER_ReactionReq;
		if (p_type.Equals("Message.OPLAYER_ReactionResp")) return Messages.OPLAYER_ReactionResp;
		if (p_type.Equals("Message.CONTROLLER_AssignId")) return Messages.CONTROLLER_AssignId;
        if (p_type.Equals("Message.CONTROLLER_TurnEnd")) return Messages.CONTROLLER_TurnEnd;
        if (p_type.Equals("Message.CONTROLLER_TurnMasterRightsInd")) return Messages.CONTROLLER_TurnMasterRightsInd;
        if (p_type.Equals("Message.PLAYER_DisableReactionReq")) return Messages.PLAYER_DisableReactionReq;
        if (p_type.Equals("Message.PLAYER_DisableReactionResp")) return Messages.PLAYER_DisableReactionResp;
        if (p_type.Equals("Message.OPLAYER_FinalActionReq")) return Messages.OPLAYER_FinalActionReq;
        if (p_type.Equals("Message.OPLAYER_FinalActionResp")) return Messages.OPLAYER_FinalActionResp;
        if (p_type.Equals("Message.PLAYER_FinalActionAck")) return Messages.PLAYER_FinalActionAck;
        if (p_type.Equals("Message.PLAYER_FinalActionReq")) return Messages.PLAYER_FinalActionReq;
        if (p_type.Equals("Message.PLAYER_FinalActionResp")) return Messages.PLAYER_FinalActionResp;

        return Messages.NONE;
    }

    public static Card getCardType(Card p_card)
    {
        switch (p_card.m_action)
        {
            case Actions.Police:
                return new Police(p_card);

            case Actions.Russia:
                return new Russia(p_card);

            case Actions.Protest:
                return new Protest(p_card);

            case Actions.UE:
                return new UE(p_card);

            case Actions.Media:
                return new Media(p_card);

            case Actions.ONZ:
                return new ONZ(p_card);

            default:
                return null;
        }
    }

    public static PlayerHand getPlayer(Players p_player, List<PlayerHand> p_players)
    {

        return p_players.Find(p_x => p_x.m_playerId.Equals(p_player));
    }

    public static PlayerHand getPlayer(Controller p_controller, Players p_player)
    {
        switch (p_player)
        {
            case Players.Player1:
                if (p_controller.m_players[0] != null)
                    return p_controller.m_players[0];
                break;

            case Players.Player2:
                if (p_controller.m_players[1] != null)
                    return p_controller.m_players[1];
                break;

            case Players.Player3:
                if (p_controller.m_players[2] != null)
                    return p_controller.m_players[2];
                break;

            case Players.Player4:
                if (p_controller.m_players[3] != null)
                    return p_controller.m_players[3];
                break;

            case Players.Player5:
                if (p_controller.m_players[4] != null)
                    return p_controller.m_players[4];
                break;

            case Players.Player6:
                if (p_controller.m_players[5] != null)
                    return p_controller.m_players[5];
                break;
        }
        return null;
    }

    public static Players setPlayerId(List<PlayerHand> p_playersList)
    {
        if (p_playersList.Count < 7)
            return (Players)p_playersList.Count - 1;
        return Players.UNSUPPORTED;
    }
};