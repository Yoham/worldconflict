﻿using System.Runtime.CompilerServices;

[System.Serializable]
public class Cards
{
    public delegate void CardChanged(Cards p_cards);
    public event CardChanged OnCardChanged;

    private Card m_first;// { get; set; }
    private Card m_second;// { get; set; }
    public Card First
    {
        get { return m_first; }
        set
        {
            m_first = value;
            if (OnCardChanged != null)
                OnCardChanged(this);


        }
    }
    public Card Second
    {
        get { return m_second; }
        set
        {
            m_second = value;
            if (OnCardChanged != null)
                OnCardChanged(this);

        }
    }

    public Cards(Card first, Card second)
    {
        this.First = first;
        this.Second = second;
    }

};