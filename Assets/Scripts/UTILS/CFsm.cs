﻿using System.Collections;
using Enum;
using UnityEngine;

namespace Control
{
    [System.Serializable]
    public class CFSM : BaseFsm
    {
        protected CFSM() : base()
        {
        }

        protected override void sendMessage<T>(T p_messageType, Players p_player)
        {
            Debug.Log("Sending message " + typeof(T) +" " + p_messageType.ToString());
            ServerPort.send(p_messageType, p_player);
        }

        protected override IEnumerator getMessage<T>()
        {
            yield return new WaitForSeconds(0.5f);
            object l_received = ServerPort.receive<object>();
            yield return l_received;
            if (l_received != null)
            {
                if (l_received.GetType() == typeof (T))
                {
                    markReceived();
                }
            }
            yield break;
        }

        protected override void markReceived()
        {
            ServerPort.markProceeded();
        }
    }
}